package tw.teddysoft.ezddd.data.io.ezes.relay;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Path;

public interface BufferedWriterFactory {
    BufferedWriter createWriter(Path path) throws IOException;
}
