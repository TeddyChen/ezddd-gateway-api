package tw.teddysoft.ezddd.data.io.ezes.relay;

import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.data.adapter.in.relay.ActiveEventRelay;
import tw.teddysoft.ezddd.data.adapter.in.relay.ContinuationMode;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageData;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDbClient;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;

public class EzesCatchUpRelay<Message> extends ActiveEventRelay<MessageData, Message, MessageProducer<Message>> {

    public static final int DEFAULT_WINDOW_SIZE = 50;

    public static final int DEFAULT_POLLING_INTERVAL = 500;

    public static final long INITIAL_CHECKPOINT_VALUE = 1;

    private final MessageDbClient messageDbClient;

    private final Path checkpointPath;
    // MILLISECONDS
    private final int pollingInterval;
    private final BufferedWriterFactory bufferedWriterFactory;

    private volatile boolean keepRunning;

    private final int windowSize;

    public record RelayConfiguration<Message>(
        MessageDbClient messageDbClient,
        MessageProducer<Message> eventProducer,
        Path checkpointPath,
        Converter<MessageData, Message> converter,
        int pollingInterval,
        int windowSize,
        ContinuationMode continuationMode,
        BufferedWriterFactory bufferedWriterFactory
    ) {

        public static <Message> RelayConfiguration<Message> of(
            MessageDbClient messageDbClient,
            MessageProducer<Message> eventProducer,
            Path checkpointPath,
            Converter<MessageData, Message> converter
        ) {
            return new RelayConfiguration<>(
                messageDbClient,
                eventProducer,
                checkpointPath,
                converter,
                DEFAULT_POLLING_INTERVAL,
                DEFAULT_WINDOW_SIZE,
                ContinuationMode.CONTINUE,
                new FilesBufferedWriterFactory()
            );
        }

        public RelayConfiguration<Message> withPollingInterval(int newPollingInterval) {
            return new RelayConfiguration<>(
                messageDbClient,
                eventProducer,
                checkpointPath,
                converter,
                newPollingInterval,
                windowSize,
                continuationMode,
                bufferedWriterFactory
            );
        }

        public RelayConfiguration<Message> withWindowSize(int newWindowSize) {
            return new RelayConfiguration<>(
                messageDbClient, 
                eventProducer,
                checkpointPath,
                converter,
                pollingInterval,
                newWindowSize,
                continuationMode,
                bufferedWriterFactory
            );
        }

        public RelayConfiguration<Message> withContinuationMode(ContinuationMode newMode) {
            return new RelayConfiguration<>(
                messageDbClient,
                eventProducer,
                checkpointPath,
                converter,
                pollingInterval,
                windowSize,
                newMode,
                bufferedWriterFactory
            );
        }

        public RelayConfiguration<Message> withBufferedWriterFactory(BufferedWriterFactory newBufferedWriterFactory) {
            return new RelayConfiguration<>(
                messageDbClient,
                eventProducer,
                checkpointPath,
                converter,
                pollingInterval,
                windowSize,
                continuationMode,
                newBufferedWriterFactory
            );
        }
    }

    public EzesCatchUpRelay(RelayConfiguration<Message> config) {
        super(config.eventProducer(), config.converter());
        this.messageDbClient = config.messageDbClient();
        this.checkpointPath = config.checkpointPath();
        this.pollingInterval = config.pollingInterval();
        this.windowSize = config.windowSize();
        this.continuationMode = config.continuationMode();
        this.bufferedWriterFactory = config.bufferedWriterFactory();
        this.keepRunning = true;
    }

    @Override
    public void run() {
        System.out.println("Ezes catch up relay starts: " + Thread.currentThread());

        try {
            Checkpoint checkpoint = initializeCheckpoint();
            processMessages(checkpoint);
        } catch (Exception e) {
            handleFatalError(e);
        }
    }

    @Override
    public void shutdown() {
        keepRunning = false;
    }

    private Checkpoint initializeCheckpoint() {
        return Files.exists(checkpointPath) ?
            Checkpoint.of(checkpointPath) :
            Checkpoint.of(INITIAL_CHECKPOINT_VALUE);
    }

    private void processMessages(Checkpoint checkpoint) {
        while (!Thread.currentThread().isInterrupted() && keepRunning) {
            try {
                waitInterval();
                checkpoint = processMessageBatch(checkpoint);
                updateCheckpoint(checkpoint);
            } catch (InterruptedException ie) {
                handleInterruptException();
            } catch (Exception e) {
                handleException(e);
            }
        }
    }

    private void handleFatalError(Exception e) {
        // TODO: log exception
        e.printStackTrace();
    }

    private void waitInterval() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(pollingInterval);
    }

    private Checkpoint processMessageBatch(Checkpoint checkpoint){
        List<MessageData> streams = messageDbClient.findAllStreamFromPosition(checkpoint.value(), windowSize);

        for (MessageData stream : streams) {
            checkpoint = checkpoint.increase();
            eventProducer.post(converter.convert(stream));
        }

        return checkpoint;
    }

    private void handleInterruptException() {
        keepRunning = false;
        Thread.currentThread().interrupt();
    }

    private void handleException(Exception e) {
        e.printStackTrace();
        if (ContinuationMode.TERMINATION == continuationMode) {
            keepRunning = false;
            Thread.currentThread().interrupt();
        }
    }

    private void updateCheckpoint(Checkpoint checkpoint) throws IOException {
        Path tempFile = Files.createTempFile("temp", ".txt");
        try (BufferedWriter writer = bufferedWriterFactory.createWriter(tempFile)) {
            writer.write(checkpoint.value().toString());
        }

        Files.move(tempFile, checkpointPath, StandardCopyOption.REPLACE_EXISTING);
    }
    record Checkpoint(Long value) {
        public static Checkpoint of(Path path) {
            String checkpointStr = "";
            try {
                checkpointStr = Files.readString(path);
                return new Checkpoint(Long.parseLong(checkpointStr));
            } catch (NoSuchFileException e) {
                throw new CheckpointException("Checkpoint file not found.", e);
            } catch (IOException e) {
                throw new CheckpointException(e);
            } catch (NumberFormatException e) {
                new CheckpointException(format("Invalid Checkpoint format: %s", checkpointStr), e).printStackTrace();
                return new Checkpoint(INITIAL_CHECKPOINT_VALUE);
            }
        }

        public static Checkpoint of(long value) {
            return new Checkpoint(value);
        }

        public Checkpoint increase(long size) {
            return new Checkpoint(value + size);
        }

        public Checkpoint increase() {
            return increase(1);
        }

    }

    static class CheckpointException extends RuntimeException {
        public CheckpointException(Throwable cause) {
            super(cause);
        }

        public CheckpointException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
