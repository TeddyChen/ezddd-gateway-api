package tw.teddysoft.ezddd.data.io.ezes.relay;

import org.json.JSONException;
import org.postgresql.PGNotification;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.data.adapter.in.relay.ActiveEventRelay;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;
import java.sql.*;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class EzesVolatileRelay<Message> extends ActiveEventRelay<PGNotification, Message, MessageProducer<Message>> {

    private final Connection conn;
    private final org.postgresql.PGConnection pgconn;

    // MILLISECONDS
    private int poolingInterval = 500;

    private boolean keepRunning = true;

    public EzesVolatileRelay(String connectionString,
                             String user,
                             String password,
                             int poolingInterval,
                             MessageProducer<Message> eventProducer,
                             Converter<PGNotification, Message> converter) throws SQLException {
        super(eventProducer, converter);
        this.conn = DriverManager.getConnection(connectionString, user, password);
        this.pgconn = (org.postgresql.PGConnection) conn;
        this.poolingInterval = poolingInterval;
        try (Statement stmt = conn.createStatement()) {
            stmt.execute("LISTEN domain_event");
        }
    }

    @Override
    public void run() {
        System.out.println("Postgres listener starts");

        while (!Thread.currentThread().isInterrupted() && keepRunning) {
            try {
                // issue a dummy query to contact the backend
                // and receive any pending notifications.
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT 1");
                rs.close();
                stmt.close();

                PGNotification[] notifications = pgconn.getNotifications();
                if (Objects.nonNull(notifications)) {
                    for (PGNotification notification : notifications) {
                        eventProducer.post(converter.convert(notification));
                    }
                }

                // wait a while before checking again for new notifications
                TimeUnit.MILLISECONDS.sleep(poolingInterval);
            } catch (SQLException | JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException ie) {
                keepRunning = false;
                Thread.currentThread().interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void close(){
        try {
            if(null != pgconn)
                pgconn.cancelQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (null != conn)
                conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            eventProducer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void shutdown(){
        keepRunning = false;
        close();
    }
}
