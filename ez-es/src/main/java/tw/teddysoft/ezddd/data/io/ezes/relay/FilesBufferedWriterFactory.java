package tw.teddysoft.ezddd.data.io.ezes.relay;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class FilesBufferedWriterFactory implements BufferedWriterFactory{
    @Override
    public BufferedWriter createWriter(Path path) throws IOException {
        return Files.newBufferedWriter(path);
    }
}
