package tw.teddysoft.ezddd.data.io.ezes.relay;

import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageData;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDataMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;

public class MessageDbToDomainEventDataConverter implements Converter<MessageData, DomainEventData> {

    @Override
    public DomainEventData convert(MessageData message) {
        return MessageDataMapper.toDomainEventData(message);
    }
}
