package tw.teddysoft.ezddd.data.io.ezes.relay;

import org.json.JSONObject;
import org.postgresql.PGNotification;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class PgToDomainEventDataConverter implements Converter<PGNotification, DomainEventData> {
    @Override
    public DomainEventData convert(PGNotification event) {
        JSONObject domainEventJson = new JSONObject(event.getParameter());
        String eventType = domainEventJson.getString("type");
        JSONObject data = domainEventJson.getJSONObject("data");
        String id = data.getString("id");

        return new DomainEventData(
                UUID.fromString(id),
                eventType,
                "application/json",
                data.toString().getBytes(StandardCharsets.UTF_8),
                "{}".getBytes(StandardCharsets.UTF_8));
    }
}
