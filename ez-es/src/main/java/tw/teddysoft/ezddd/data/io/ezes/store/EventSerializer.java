package tw.teddysoft.ezddd.data.io.ezes.store;

import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEvent;


public class EventSerializer {

	private static EventSerializer eventSerializer;

    public static synchronized EventSerializer instance() {
        if (EventSerializer.eventSerializer == null) {
            EventSerializer.eventSerializer = new EventSerializer();
        }

        return EventSerializer.eventSerializer;
    }
    
    public static String serialize(DomainEvent domainEvent) {
//        String eventBody = new Gson().toJson(domainEvent);

        String eventBody = Json.asString(domainEvent);
        return eventBody;
    }

    public static <T extends DomainEvent> T deserialize(String eventBody, final Class<T> eventType) {
//        T domainEvent = new Gson().fromJson(eventBody, eventType);

        T domainEvent = Json.readValue(eventBody, eventType);
        return domainEvent;
    }

}
