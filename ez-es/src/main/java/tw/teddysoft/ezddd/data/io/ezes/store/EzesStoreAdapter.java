package tw.teddysoft.ezddd.data.io.ezes.store;

import tw.teddysoft.ezddd.data.adapter.repository.es.EventStore;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;

import java.util.List;
import java.util.Optional;

public class EzesStoreAdapter implements EventStore {

    private final MessageDbClient client;

    public EzesStoreAdapter(MessageDbClient client) {
        this.client = client;
    }

    @Override
    public void save(EventStoreData eventStoreData) {
        if (null == eventStoreData) {
            throw new RuntimeException("AggregateData cannot be null.");
        }

        long version = client.saveDomainEvents(eventStoreData);
        eventStoreData.setVersion(version);
    }

    @Override
    public Optional<EventStoreData> getEventsByType(String eventType) {
        return getEventsByStreamName(EVENT_TYPE_PREFIX + eventType);
    }

    @Override
    public Optional<EventStoreData> getEventsByStreamName(String streamName) {
        EventStoreData eventStoreData = new EventStoreData();
        eventStoreData.setStreamName(streamName);
        List<MessageData> messageDatas = client.findByStreamName(streamName);

        messageDatas.forEach(x -> {
            eventStoreData.getDomainEventDatas().add(MessageDataMapper.toDomainEventData(x));
        });
        eventStoreData.setVersion(eventStoreData.getDomainEventDatas().size() - 1);

        return Optional.of(eventStoreData);
    }
}
