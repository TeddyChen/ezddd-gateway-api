package tw.teddysoft.ezddd.data.io.ezes.store;

import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

public class MessageDataMapper {

    public static final String RAW_TYPE = "rawType";
    public static final String JSON_CONTENT_TYPE = "application/json";

    private static DomainEventTypeMapper mapper = null;

    public static DomainEventTypeMapper getMapper() {
        return mapper;
    }

    public static void setMapper(DomainEventTypeMapper newMapper) {
        mapper = newMapper;
    }

//      TODO: unused. discuss to remove
//    public static MessageData toData(DomainEvent event) {
//        requireNotNull("DomainEvent", event);
//        requireNotNull("Please call setMapper to config public class DomainEventMapper first", mapper);
//
//        return new MessageData(
//                event.id(),
//                Json.asString(event),
//                mapper.toMappingType(event),
//                "",
//                event.occurredOn(),
//                getEventRawTypeMetadata(event)
//        );
//    }
//
//    public static List<MessageData> toData(List<DomainEvent> events) {
//        requireNotNull("DomainEvent", events);
//        requireNotNull("Please call setMapper to config public class DomainEventMapper first", mapper);
//
//        return events.stream().map(MessageDataMapper::toData).collect(Collectors.toList());
//    }

    public static <T extends DomainEvent> T toDomain(MessageData data) {
        requireNotNull("DomainEventData", data);
        requireNotNull("Please call setMapper to config public class DomainEventMapper first", mapper);

        T domainEvent = null;
        try {
            domainEvent = (T) Json.readValue(data.getEventBody(), mapper.toClass(data.getEventType()));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return domainEvent;
    }

    public static <T extends DomainEvent> List<T> toDomain(List<MessageData> datas) {
        requireNotNull("DomainEventData list", datas);

        List<T> result = new ArrayList<>();
        datas.forEach( x -> result.add(toDomain(x)));
        return result;
    }

    public static DomainEventData toDomainEventData(MessageData messageData) {
        requireNotNull("MessageData", messageData);

        return new DomainEventData(
                messageData.getId(),
                messageData.getEventType(),
                JSON_CONTENT_TYPE,
                messageData.getEventBody().getBytes(StandardCharsets.UTF_8),
                messageData.getMetadata().getBytes(StandardCharsets.UTF_8));
    }

    public static List<DomainEventData> toDomainEventData(List<MessageData> messageDatas) {
        requireNotNull("MessageData", messageDatas);

        return messageDatas.stream().map(MessageDataMapper::toDomainEventData).collect(Collectors.toList());
    }

//      TODO: unused. discuss to remove
//    private static String getEventRawTypeMetadata(DomainEvent event) {
//        DomainEventMetadata metadata = new DomainEventMetadata();
//        metadata.append(RAW_TYPE, event.getClass().getName());
//
//        return metadata.asJsonString();
//    }
}
