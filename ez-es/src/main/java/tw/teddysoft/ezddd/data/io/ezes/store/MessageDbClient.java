package tw.teddysoft.ezddd.data.io.ezes.store;

import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;

import java.util.List;

public interface MessageDbClient {

    long writeMessage(String id,
                      String streamName,
                      String type,
                      String data,
                      String metadata,
                      Long expectedVersion);

    long writeMessage(String id,
                      String streamName,
                      String type,
                      String data,
                      String metadata);

    long getStreamVersion(String streamName);

    String getMessageStoreVersion();

    List<MessageData> findByStreamName(String streamName);

    List<MessageData> findByAggregateStreamName(String streamName);

    List<MessageData> findByEventType(String eventType);

    List<MessageData> findByCategory(String category);

    List<MessageData> findAllStream();

    List<MessageData> findByStreamNameAndEventTypeIn(String streamName, List<String> types);

    long saveDomainEvents(EventStoreData eventStoreData);

    void saveDomainEventsWithoutVersion(EventStoreData eventStoreData);

    List<MessageData> findAllStreamFromPosition(long nextPosition);

    List<MessageData> findAllStreamFromPosition(long nextPosition, int windowSize);
}
