package tw.teddysoft.ezddd.data.io.ezes.store;

import org.json.JSONObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;

import java.util.List;
@NoRepositoryBean
public interface PgMessageDbClient extends JpaRepository<MessageData, String>, MessageDbClient {

    @Query(nativeQuery = true, value = "SELECT message_store.write_message(:id, :stream_name, :type, CAST(:data as JSONB), CAST(:metadata as JSONB), :expected_version)")
    long writeMessage(@Param("id") String id,
                      @Param("stream_name") String streamName,
                      @Param("type") String type,
                      @Param("data") String data,
                      @Param("metadata") String metadata,
                      @Param("expected_version") Long expectedVersion);

    @Query(nativeQuery = true, value = "SELECT message_store.write_message(:id, :stream_name, :type, CAST(:data as JSONB), CAST(:metadata as JSONB))")
    long writeMessage(@Param("id") String id,
                      @Param("stream_name") String streamName,
                      @Param("type") String type,
                      @Param("data") String data,
                      @Param("metadata") String metadata);

    @Query(nativeQuery = true, value = "select message_store.stream_version(CAST(:stream_name as VARCHAR))")
    long getStreamVersion(@Param("stream_name") String streamName);


    @Query(nativeQuery = true, value = "select message_store.message_store_version()")
    String getMessageStoreVersion();

    default List<MessageData> findByStreamName(String streamName) {
        if (streamName.charAt(0) != '$') {
            return findByAggregateStreamName(streamName);
        }
        else if (streamName.substring(0, 4).equals("$et-")) {
            return findByEventType(streamName.substring(4));
        }
        else if (streamName.substring(0, 4).equals("$ce-")) {
            return findByCategory(streamName.substring(4) + "-%");
        }
        else if (streamName.substring(0, 4).equals("$all")) {
            return findAllStream();
        }
        else {
            throw new RuntimeException("Unsupported stream name: " + streamName);
        }
    }

    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position 
             FROM messages AS m 
             WHERE m.stream_name = :stream_name 
             ORDER BY m.global_position ASC
             """, nativeQuery = true)
    List<MessageData> findByAggregateStreamName(@Param("stream_name") String streamName);

    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position 
             FROM messages AS m 
             WHERE m.type = :type 
             ORDER BY m.global_position ASC
             """, nativeQuery = true)
    List<MessageData> findByEventType(@Param("type") String eventType);

    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position 
             FROM messages AS m 
             WHERE m.stream_name LIKE :category 
             ORDER BY m.global_position ASC
             """, nativeQuery = true)
    List<MessageData> findByCategory(@Param("category") String category);

    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position 
             FROM messages AS m
             ORDER BY m.global_position ASC
             """, nativeQuery = true)
    List<MessageData> findAllStream();

    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position 
             FROM messages AS m
             WHERE m.global_position >= :global_position
             ORDER BY m.global_position ASC
             """, nativeQuery = true)
    List<MessageData> findAllStreamFromPosition(@Param("global_position") long globalPosition);

    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position 
             FROM messages AS m
             WHERE m.global_position >= :global_position
             ORDER BY m.global_position ASC
             LIMIT :window_size
             """, nativeQuery = true)
    List<MessageData> findAllStreamFromPosition(@Param("global_position") long globalPosition, @Param("window_size") int windowSize);


    @Query(value = """
             SELECT id, CAST(data AS TEXT), type, stream_name, time, CAST(metadata AS TEXT), global_position, position
             FROM messages AS m
             WHERE m.stream_name = :streamName
                 AND m.type IN :types
             ORDER BY m.global_position ASC
             """, nativeQuery = true)
    List<MessageData> findByStreamNameAndEventTypeIn(@Param("streamName") String streamName, @Param("types") List<String> types);

    @Transactional
    default long saveDomainEvents(EventStoreData eventStoreData){
        long expectedVersion = eventStoreData.getOptimisticLockVersion();

        for(var event : eventStoreData.getDomainEventDatas()){
            expectedVersion = writeMessage(event.id().toString(),
                    eventStoreData.getStreamName(),
                    event.eventType(),
                    new JSONObject(new String(event.eventBody())).toString(),
                    String.valueOf(new JSONObject("{}")),
                    expectedVersion);
        }
        return expectedVersion;
    }

    @Transactional
    default void saveDomainEventsWithoutVersion(EventStoreData eventStoreData){

        for(var event : eventStoreData.getDomainEventDatas()){
            writeMessage(event.id().toString(),
                    eventStoreData.getStreamName(),
                    event.eventType(),
                    new JSONObject(new String(event.eventBody())).toString(),
                    String.valueOf(new JSONObject("{}")));
        }

    }
}
