package tw.teddysoft.ezddd.data.io.ezes;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.data.adapter.repository.es.EsRepositoryPeerAdapter;
import tw.teddysoft.ezddd.data.adapter.repository.es.EventStore;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.out.repository.Repository;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EsRepository;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;
import tw.teddysoft.ezddd.data.io.ezes.store.EzesStoreAdapter;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageData;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDataMapper;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDbClient;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureAfter({TestConfiguration.class})
@TestPropertySource(locations = "classpath:test.properties")
@ContextConfiguration(classes= JpaApplicationTestContext.class)
@SpringBootTest
@Testcontainers
public class EzesStoreAdapterTest {
    @Container
    public static PostgreSqlMessageDbContainer postgresContainer = new PostgreSqlMessageDbContainer("account");

    @Autowired
    public MessageDbClient messageDbClient;

    @BeforeAll
    public static void beforeAll() {
        DomainEventMapper.setMapper(UserEvents.mapper());
    }

    @DynamicPropertySource
    public static void configureProperties(DynamicPropertyRegistry registry) {
        System.out.println("***********JDBC URL!!!**********");
        System.out.println(postgresContainer.getJdbcUrl());
        registry.add("spring.datasource.kanban.url", postgresContainer::getJdbcUrl);
        registry.add("jdbc.test.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.kanban.username", postgresContainer::getUsername);
        registry.add("spring.datasource.kanban.password", postgresContainer::getPassword);
    }


    @Test
    public void save_and_read() {
        String userId = UUID.randomUUID().toString();
        String username = "teddy";
        String password = "1234";
        String email = "teddy@gmail.com";
        String nickname = "Teddy";

        User user = new User(userId, username, password, email, nickname, Role.User, true);
        user.changeNickname("Ada");
        user.changeEmail("ada@gmail.com");

        EventStore eventStore = new EzesStoreAdapter(messageDbClient);
        RepositoryPeer<EventStoreData, String> repositoryPeer = new EsRepositoryPeerAdapter(eventStore);
        Repository<User, String> repository = new EsRepository<>(repositoryPeer, User.class, User.CATEGORY);
        assertEquals(3, user.getDomainEventSize());
        repository.save(user);
        assertEquals(0, user.getDomainEventSize());

        User modifiedUser = repository.findById(userId).get();

        assertEquals(username, modifiedUser.getUsername());
        assertEquals(password, modifiedUser.getPassword());
        assertEquals("Ada", modifiedUser.getNickname());
        assertEquals("ada@gmail.com", modifiedUser.getEmail());

        List<MessageData> messageDatas = messageDbClient.findAllStream();
        EventStoreData eventStoreData = new EventStoreData();

        messageDatas.forEach(x -> {
            eventStoreData.getDomainEventDatas().add(MessageDataMapper.toDomainEventData(x));
        });

        assertEquals(3, eventStoreData.getDomainEventDatas().size());
        assertEquals("UserEvents$UserRegistered", eventStoreData.getDomainEventDatas().get(0).eventType());
        assertEquals("UserEvents$NicknameChanged", eventStoreData.getDomainEventDatas().get(1).eventType());
        assertEquals("UserEvents$EmailChanged", eventStoreData.getDomainEventDatas().get(2).eventType());

        modifiedUser.changePassword("new_password");

        repository.save(modifiedUser);

        User modifiedUser2 = repository.findById(userId).get();

        assertEquals("new_password", modifiedUser2.getPassword());
    }
}
