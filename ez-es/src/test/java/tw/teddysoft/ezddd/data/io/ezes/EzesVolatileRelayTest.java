package tw.teddysoft.ezddd.data.io.ezes;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.postgresql.PGNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.BlockingMessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.EventBusProducer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreMapper;
import tw.teddysoft.ezddd.data.io.ezes.relay.PgToDomainEventDataConverter;
import tw.teddysoft.ezddd.data.io.ezes.relay.EzesVolatileRelay;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDbClient;

import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.isA;

@AutoConfigureAfter({TestConfiguration.class})
@TestPropertySource(locations = "classpath:test.properties")
@ContextConfiguration(classes= JpaApplicationTestContext.class)
@SpringBootTest
@Testcontainers
public class EzesVolatileRelayTest {

    @Container
    public static PostgreSqlMessageDbContainer postgresContainer = new PostgreSqlMessageDbContainer("account");

    @Autowired
    public MessageDbClient messageDbClient;
    private EzesVolatileRelay ezesVolatileRelay;
    public MessageBus<DomainEventData> messageBus;
    public DomainEventTypeMapper domainEventTypeMapper;
    private FakeAllEventsListener.MockitoEventHandler mockitoEventsHandler;


    @DynamicPropertySource
    public static void configureProperties(DynamicPropertyRegistry registry) {
        System.out.println("***********JDBC URL!!!**********");
        System.out.println(postgresContainer.getJdbcUrl());
        registry.add("spring.datasource.kanban.url", postgresContainer::getJdbcUrl);
        registry.add("jdbc.test.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.kanban.username", postgresContainer::getUsername);
        registry.add("spring.datasource.kanban.password", postgresContainer::getPassword);
    }

    @BeforeEach
    public void setup() throws SQLException {
        mockitoEventsHandler = Mockito.mock(FakeAllEventsListener.MockitoEventHandler.class);
        domainEventTypeMapper = UserEvents.mapper();
        DomainEventMapper.setMapper(UserEvents.mapper());
        messageBus = new BlockingMessageBus();
        messageBus.register(new FakeAllEventsListener(mockitoEventsHandler));
        Converter<PGNotification, DomainEventData> converter = new PgToDomainEventDataConverter();
        ezesVolatileRelay = new EzesVolatileRelay(
                postgresContainer.getJdbcUrl(),
                "postgres",
                "root",
                20,
                new EventBusProducer(messageBus),
                converter);
        new Thread(ezesVolatileRelay).start();
    }

    @AfterEach
    public void teardown() {
        if (null != ezesVolatileRelay)
            ezesVolatileRelay.shutdown();
    }

    @Test
    public void will_receive_a_event_from_esdb_and_publish_it_to_the_event_bus() throws ExecutionException, InterruptedException {

        String userId = UUID.randomUUID().toString();
        String username = "teddy";
        String password = "1234";
        String email = "teddy@gmail.com";
        String nickname = "Teddy";

        User user = new User(userId, username, password, email, nickname, Role.User, true);

        messageDbClient.saveDomainEvents(EventStoreMapper.toData(user));

        await().untilAsserted(() -> Mockito.verify(mockitoEventsHandler, Mockito.times(1)).execute(isA(UserEvents.UserRegistered.class)));
    }

    private static class FakeAllEventsListener implements Reactor<DomainEventData> {
        private final MockitoEventHandler mockitoEventHandler;

        public FakeAllEventsListener(MockitoEventHandler mockitoEventHandler) {
            this.mockitoEventHandler = mockitoEventHandler;
        }

        @Override
        public void execute(DomainEventData eventData) {
            this.mockitoEventHandler.execute(DomainEventMapper.toDomain(eventData));
        }

        private static class MockitoEventHandler {
            public void execute(DomainEvent event) {
                throw new UnsupportedOperationException();
            }
        }
    }
}