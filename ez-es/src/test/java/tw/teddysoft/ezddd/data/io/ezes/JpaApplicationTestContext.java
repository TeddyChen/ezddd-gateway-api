package tw.teddysoft.ezddd.data.io.ezes;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@EntityScan(basePackages={"tw.teddysoft.ezddd.data.io.ezes"})
@SpringBootApplication(scanBasePackages={"tw.teddysoft.ezddd.data.io.ezes"})
public abstract class JpaApplicationTestContext {
}
