package tw.teddysoft.ezddd.data.io.ezes;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDbClient;
import tw.teddysoft.ezddd.data.io.ezes.store.PgMessageDbClient;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@Configuration("DataSourceConfiguration")
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"tw.teddysoft.ezddd.data.io.ezes.store"},
        entityManagerFactoryRef = "kanbanEntityManagerFactory",
        transactionManagerRef = "kanbanTransactionManager"
)
public class TestConfiguration {
    public static final String[] ENTITY_PACKAGES = {"tw.teddysoft.ezddd.data.io.ezes.store"};

    @Bean(name = "entityManagerInBoard")
    public EntityManager getEntityManager(final @Qualifier("kanbanEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
        return entityManagerFactoryBean.getObject().createEntityManager();
    }

    @Bean(name = "messageDbClient")
    public MessageDbClient messageDbClient(final @Qualifier("entityManagerInBoard") EntityManager entityManager) {
        RepositoryFactorySupport factory = new JpaRepositoryFactory(entityManager);
        return factory.getRepository(PgMessageDbClient.class);
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.kanban")
    public DataSourceProperties kanbanDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.kanban.configuration")
    public DataSource kanbanDataSource() {
        return kanbanDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();

    }

    @Bean(name = "kanbanEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean kanbanEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(kanbanDataSource())
                .packages(ENTITY_PACKAGES)
                .build();
    }

    @Bean
    public PlatformTransactionManager kanbanTransactionManager(
            final @Qualifier("kanbanEntityManagerFactory") LocalContainerEntityManagerFactoryBean kanbanEntityManagerFactory) {
        return new JpaTransactionManager(kanbanEntityManagerFactory.getObject());
    }
}
