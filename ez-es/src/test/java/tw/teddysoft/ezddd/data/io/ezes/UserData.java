package tw.teddysoft.ezddd.data.io.ezes;

import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox.OutboxData;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class UserData implements OutboxData<String> {

	@Transient
	private String streamName;

	@Transient
	private List<DomainEventData> domainEventDatas;

	@Id
	@Column(name="id")
	private String userId;

	@Column(name="username")
	private String username;

	@Column(name="email")
	private String email;

	@Column(name="password")
	private String password;

	@Column(name="nickname")
	private String nickname;

	@Column(name="role")
	private String role;

	@Column(name="activated")
	private boolean activated;

	@Column(columnDefinition = "bigint DEFAULT 0", nullable = false)
	private long version;

	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	@Transient
	public String getId() {
		return userId;
	}

	@Override
	@Transient
	public void setId(String id) {
		this.userId = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	@Transient
	public List<DomainEventData> getDomainEventDatas() {
		return domainEventDatas;
	}

	@Override
	@Transient
	public void setDomainEventDatas(List<DomainEventData> domainEventDatas) {
		this.domainEventDatas = domainEventDatas;
	}

	@Override
	@Transient
	public String getStreamName() {
		return streamName;
	}

	@Override
	@Transient
	public void setStreamName(String streamName) {
		this.streamName = streamName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getPassword() {
		return password;
	}
}
