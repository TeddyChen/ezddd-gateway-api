package tw.teddysoft.ezddd.data.io.ezes;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

public interface UserEvents extends DomainEvent {

    String userId();

    default String aggregateId(){
        return userId();
    }

    record UserRegistered(
            String userId,
            String username,
            String password,
            String email,
            String nickname,
            Role role,
            Boolean isActivated,
            UUID id,
            Instant occurredOn
    ) implements UserEvents, ConstructionEvent {
        public UserRegistered {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(username);
            Objects.requireNonNull(password);
            Objects.requireNonNull(email);
            Objects.requireNonNull(nickname);
            Objects.requireNonNull(role);
            Objects.requireNonNull(isActivated);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record EmailChanged(
            String userId,
            String email,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public EmailChanged {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(email);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record NicknameChanged(
            String userId,
            String nickname,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public NicknameChanged {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(nickname);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record PasswordChanged(
            String userId,
            String password,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public PasswordChanged {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(password);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record UserLoggedIn(
            String userId,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public UserLoggedIn {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record UserLoggedInFail(
            String userId,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public UserLoggedInFail {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record UserActivated(
            String executorUserId,
            String userId,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public UserActivated {
            Objects.requireNonNull(executorUserId);
            Objects.requireNonNull(userId);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    record Unregistered(
            String executorUserId,
            String userId,
            UUID id,
            Instant occurredOn
    ) implements UserEvents {
        public Unregistered {
            Objects.requireNonNull(executorUserId);
            Objects.requireNonNull(userId);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////

    record UserDeleted(
            String userId,
            UUID id,
            Instant occurredOn
    ) implements UserEvents, DestructionEvent {
        public UserDeleted {
            Objects.requireNonNull(userId);
            Objects.requireNonNull(id);
            Objects.requireNonNull(occurredOn);
        }
    }

    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////

    class TypeMapper extends DomainEventTypeMapper.DefaultMapper {
        public static final String MAPPING_TYPE_PREFIX = "UserEvents$";

        public static final String USER_REGISTERED = MAPPING_TYPE_PREFIX + "UserRegistered";
        public static final String EMAIL_CHANGED = MAPPING_TYPE_PREFIX + "EmailChanged";
        public static final String NICKNAME_CHANGED = MAPPING_TYPE_PREFIX + "NicknameChanged";
        public static final String PASSWORD_CHANGED = MAPPING_TYPE_PREFIX + "PasswordChanged";
        public static final String USER_LOGGED_IN = MAPPING_TYPE_PREFIX + "UserLoggedIn";
        public static final String USER_LOGGED_IN_FAIL = MAPPING_TYPE_PREFIX + "UserLoggedInFail";
        public static final String UNREGISTERED = MAPPING_TYPE_PREFIX + "Unregistered";
        public static final String USER_DELETED = MAPPING_TYPE_PREFIX + "UserDeleted";
        public static final String USER_ACTIVATED = MAPPING_TYPE_PREFIX + "UserActivated";


        private static final DomainEventTypeMapper mapper;

        static {
            mapper = DomainEventTypeMapper.create();
            mapper.put(USER_REGISTERED, UserRegistered.class);
            mapper.put(EMAIL_CHANGED, EmailChanged.class);
            mapper.put(NICKNAME_CHANGED, NicknameChanged.class);
            mapper.put(PASSWORD_CHANGED, PasswordChanged.class);
            mapper.put(USER_LOGGED_IN, UserLoggedIn.class);
            mapper.put(USER_LOGGED_IN_FAIL, UserLoggedInFail.class);
            mapper.put(UNREGISTERED, Unregistered.class);
            mapper.put(USER_DELETED, UserDeleted.class);
            mapper.put(USER_ACTIVATED, UserActivated.class);
        }

        public static DomainEventTypeMapper getInstance(){
            return mapper;
        }

    }

    static DomainEventTypeMapper mapper(){
        return TypeMapper.getInstance();
    }

}
