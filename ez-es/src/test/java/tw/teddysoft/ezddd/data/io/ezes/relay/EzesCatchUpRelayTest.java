package tw.teddysoft.ezddd.data.io.ezes.relay;

import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import tw.teddysoft.ezddd.data.adapter.in.relay.ActiveEventRelay;
import tw.teddysoft.ezddd.data.adapter.in.relay.ContinuationMode;
import tw.teddysoft.ezddd.data.io.ezes.Role;
import tw.teddysoft.ezddd.data.io.ezes.UserEvents;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageData;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDbClient;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.BlockingMessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.EventBusProducer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.io.*;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@EzFeature
public class EzesCatchUpRelayTest {
    static String featureName = "EzesCatchUpRelay";
    static Feature feature = Feature.New(featureName);
    static final String GENERATE_CHECKPOINT = "If a checkpoint does not exist, generate a new one";
    static final String UPDATE_CHECKPOINT_FAILED = "If the relay update checkpoint fails, the checkpoint should not be updated";
    static final String FETCH_EVENT = "Fetches events according to the checkpoint and the window size";
    static final String ACK_EVENT = "If all fetch events are passed to the message producer in each polling," +
            "update the checkpoint to its current value + the number of events.";
    static final String RELAY_CONTINUATION = "If the relay encounter an exception, it may stop or continue execution";

    private Thread relayThread;
    private final Path checkpointPath = Path.of("./checkPoint.txt");
    private final int pollingInterval = 500;

    @BeforeEach
    public void setUp() {
        DomainEventMapper.setMapper(UserEvents.mapper());
        deleteFile(checkpointPath);
    }

    @BeforeAll
    public static void beforeAll() {
        feature.initialize();
        feature.NewRule(GENERATE_CHECKPOINT);
        feature.NewRule(FETCH_EVENT);
        feature.NewRule(ACK_EVENT);
        feature.NewRule(RELAY_CONTINUATION);
        feature.NewRule(UPDATE_CHECKPOINT_FAILED);
    }

    @AfterEach
    public void tearDown() {
        if (null != relayThread) {
            relayThread.interrupt();
        }
        await().untilAsserted(() -> assertFalse(relayThread.isAlive()));

        deleteFile(checkpointPath);
    }

    @EzScenario(rule = GENERATE_CHECKPOINT)
    public void database_has_no_event() {
        feature.newScenario()
                .Given("no event in MessageDb", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();

                    env.put("messageDbClient", messageDbClient);
                })
                .And("a EzesCatchUpRelay with no checkpoint", env -> {
                    deleteFile(checkpointPath);
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                                    .of(env.get("messageDbClient", MessageDbClient.class),
                                        new EventBusProducer(new BlockingMessageBus<>()),
                                        checkpointPath,
                                        new MessageDbToDomainEventDataConverter())
                                    .withPollingInterval(500);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    env.put("relay", relay);
                })
                .When("the relay starts", env -> {
                    relayThread = Thread.ofVirtual().start(env.get("relay", ActiveEventRelay.class));
                })
                .Then("the checkpoint should be generated", env -> {
                    await().untilAsserted(() -> assertTrue(Files.exists(checkpointPath)));
                })
                .And("its value should be 1", env -> {
                    EzesCatchUpRelay.Checkpoint checkpoint = EzesCatchUpRelay.Checkpoint.of(checkpointPath);
                    assertEquals(1, checkpoint.value());
                })
                .Execute();
    }

    @EzScenario(rule = GENERATE_CHECKPOINT)
    public void database_has_three_events() {
        feature.newScenario()
                .Given("3 events in MessageDb", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();
                    EventStoreData eventStoreData = new EventStoreData();
                    eventStoreData.setDomainEventDatas(List.of(
                            newUserRegisteredData("user1", "user1@email.com"),
                            newUserRegisteredData("user2", "user2@email.com"),
                            newUserRegisteredData("user3", "user3@email.com")
                    ));
                    messageDbClient.saveDomainEvents(eventStoreData);

                    env.put("messageDbClient", messageDbClient);
                })
                .And("a EzesCatchUpRelay with no checkpoint", env -> {
                    deleteFile(checkpointPath);

                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    new EventBusProducer(new BlockingMessageBus<>()),
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(500);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    env.put("relay", relay);
                })
                .When("the relay starts", env -> {
                    relayThread = Thread.ofVirtual().start(env.get("relay", ActiveEventRelay.class));
                })
                .Then("the checkpoint should be generated", env -> {
                    await().untilAsserted(() -> assertTrue(Files.exists(checkpointPath)));
                })
                .And("its value should be 4", env -> {
                    EzesCatchUpRelay.Checkpoint checkpoint = EzesCatchUpRelay.Checkpoint.of(checkpointPath);
                    assertEquals(4, checkpoint.value());
                })
                .Execute();
    }

    @EzScenario(rule = FETCH_EVENT)
    public void database_has_no_new_event() {
        feature.newScenario()
                .Given("a MessageDb with 3 events", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();
                    EventStoreData eventStoreData = new EventStoreData();
                    eventStoreData.setDomainEventDatas(List.of(
                            newUserRegisteredData("user1", "user1@email.com"),
                            newUserRegisteredData("user2", "user2@email.com"),
                            newUserRegisteredData("user3", "user3@email.com")
                    ));
                    messageDbClient.saveDomainEvents(eventStoreData);

                    env.put("messageDbClient", messageDbClient);
                })
                .And("a EzesCatchUpRelay with a message producer", env -> {
                    MessageProducer<DomainEventData> messageProducer = new FakeMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(pollingInterval);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    env.put("relay", relay);
                    env.put("messageProducer", messageProducer);
                })
                .And("the relay's checkpoint is 4", env -> {
                    createCheckpoint(checkpointPath, EzesCatchUpRelay.Checkpoint.of(4));
                })
                .When("the relay starts", env -> {
                    relayThread = Thread.ofVirtual().start(env.get("relay", ActiveEventRelay.class));
                })
                .Then("the relay should not send any event to its message producer after one polling interval", env -> {
                    wait(pollingInterval);
                    FakeMessageProducer messageProducer = env.get("messageProducer", FakeMessageProducer.class);
                    assertEquals(0, messageProducer.domainEventDatas.size());
                })
                .Execute();
    }

    @EzScenario(rule = FETCH_EVENT)
    public void database_has_new_events_and_event_size_less_than_window_size() {
        feature.newScenario()
                .Given("a MessageDb with 3 events", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();
                    EventStoreData eventStoreData = new EventStoreData();
                    eventStoreData.setDomainEventDatas(List.of(
                            newUserRegisteredData("user1", "user1@email.com"),
                            newUserRegisteredData("user2", "user2@email.com"),
                            newUserRegisteredData("user3", "user3@email.com")
                    ));
                    messageDbClient.saveDomainEvents(eventStoreData);

                    env.put("messageDbClient", messageDbClient);
                })
                .And("a EzesCatchUpRelay with window size of 5 ", env -> {
                    MessageProducer<DomainEventData> messageProducer = new FakeMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(pollingInterval)
                            .withWindowSize(5);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    env.put("relay", relay);
                    env.put("messageProducer", messageProducer);
                })
                .And("the relay's checkpoint is value 1", env -> {
                    createCheckpoint(checkpointPath, EzesCatchUpRelay.Checkpoint.of(1));
                })
                .When("the relay starts", env -> {
                    relayThread = Thread.ofVirtual().start(env.get("relay", ActiveEventRelay.class));
                })
                .Then("the relay should read 3 events from the database after one polling interval", env -> {
                    wait(pollingInterval * 2);
                    FakeMessageProducer messageProducer = env.get("messageProducer", FakeMessageProducer.class);
                    assertEquals(3, messageProducer.domainEventDatas.size());
                })
                .Execute();
    }

    @EzScenario(rule = FETCH_EVENT)
    public void database_has_new_events_and_event_size_greater_than_window_size() {
        feature.newScenario()
                .Given("a MessageDb with 10 events", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();
                    EventStoreData eventStoreData = new EventStoreData();
                    eventStoreData.setDomainEventDatas(List.of(
                            newUserRegisteredData("user1", "user1@email.com"),
                            newUserRegisteredData("user2", "user2@email.com"),
                            newUserRegisteredData("user3", "user3@email.com"),
                            newUserRegisteredData("user4", "user4@email.com"),
                            newUserRegisteredData("user5", "user5@email.com"),
                            newUserRegisteredData("user6", "user6@email.com"),
                            newUserRegisteredData("user7", "user7@email.com"),
                            newUserRegisteredData("user8", "user8@email.com"),
                            newUserRegisteredData("user9", "user9@email.com"),
                            newUserRegisteredData("user10", "user10@email.com")
                    ));
                    messageDbClient.saveDomainEvents(eventStoreData);

                    env.put("messageDbClient", messageDbClient);
                })
                .And("a EzesCatchUpRelay with window size of value 3", env -> {
                    MessageProducer<DomainEventData> messageProducer = new FakeMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(pollingInterval)
                            .withWindowSize(3);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    env.put("relay", relay);
                    env.put("messageProducer", messageProducer);
                })
                .And("the relay's checkpoint is 5", env -> {
                    createCheckpoint(checkpointPath, EzesCatchUpRelay.Checkpoint.of(5));
                })
                .When("the relay starts", env -> {
                    relayThread = Thread.ofVirtual().start(env.get("relay", ActiveEventRelay.class));
                })
                .Then("the relay should read 3 events from the database for the first polling", env -> {
                    wait((int) (pollingInterval * 1.5));
                    FakeMessageProducer messageProducer = env.get("messageProducer", FakeMessageProducer.class);
                    assertEquals(3, messageProducer.domainEventDatas.size());
                })
                .And("the relay should read 3 events from the database for the second polling", env -> {
                    wait(pollingInterval);
                    FakeMessageProducer messageProducer = env.get("messageProducer", FakeMessageProducer.class);
                    assertEquals(6, messageProducer.domainEventDatas.size());
                })
                .Execute();
    }

    @EzScenario(rule = ACK_EVENT)
    public void relay_fetches_five_events() {
        final int pollingInterval = 5000;
        feature.newScenario()
                .Given("a MessageDb with 5 events", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();
                    EventStoreData eventStoreData = new EventStoreData();
                    eventStoreData.setDomainEventDatas(List.of(
                            newUserRegisteredData("user1", "user1@email.com"),
                            newUserRegisteredData("user2", "user2@email.com"),
                            newUserRegisteredData("user3", "user3@email.com"),
                            newUserRegisteredData("user4", "user4@email.com"),
                            newUserRegisteredData("user5", "user5@email.com")));

                    messageDbClient.saveDomainEvents(eventStoreData);

                    env.put("messageDbClient", messageDbClient);
                })
                .And("a EzesCatchUpRelay with window size of value 3", env -> {
                    MessageProducer<DomainEventData> messageProducer = new FakeMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(pollingInterval)
                            .withWindowSize(3);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    env.put("relay", relay);
                    env.put("messageProducer", messageProducer);
                })
                .And("the relay's checkpoint is 1", env -> {
                    createCheckpoint(checkpointPath, EzesCatchUpRelay.Checkpoint.of(1));
                })
                .When("the relay starts", env -> {
                    relayThread = Thread.ofVirtual().start(env.get("relay", ActiveEventRelay.class));
                })
                .And("the relay completely sends the 3 events to its message producer at its first polling", env -> {
                    FakeMessageProducer messageProducer = env.get("messageProducer", FakeMessageProducer.class);
                    await().atMost(pollingInterval*2, TimeUnit.MILLISECONDS).untilAsserted(() -> assertEquals(3, messageProducer.domainEventDatas.size()));
                })
                .Then("the checkpoint should be updated to 4", env -> {
                    await().untilAsserted(() -> assertTrue(Files.exists(checkpointPath)));
                    EzesCatchUpRelay.Checkpoint checkpoint = EzesCatchUpRelay.Checkpoint.of(checkpointPath);
                    assertEquals(4, checkpoint.value());
                })
                .Execute();
    }

    @EzScenario(rule = RELAY_CONTINUATION)
    public void stop_the_relay() {
        final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        final PrintStream originalErr = System.err;
        System.setErr(new PrintStream(errContent));

                feature.newScenario()
                .Given("a MessageDb has one event", env -> {
                    MessageDbClient messageDbClient = Mockito.mock(FakeMessageDbClient.class);
                    Mockito.when(messageDbClient.findAllStreamFromPosition(1, 5)).thenReturn(List.of(
                            newUserRegisteredMessageData("user1", "user1@email.com")));

                    env.put("messageDbClient", messageDbClient);
                })
                .And("an EzesCatchUpRelay has read the event from the MessageDb", env -> {
                    MessageProducer<DomainEventData> messageProducer = new ExceptionMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(500)
                            .withWindowSize(5)
                            .withContinuationMode(ContinuationMode.TERMINATION);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    relayThread = Thread.ofVirtual().start(relay);
                    await().untilAsserted(() ->
                            Mockito.verify(env.get("messageDbClient", MessageDbClient.class),
                                    Mockito.times(1)).findAllStreamFromPosition(1, 5));
                })
//                .When("the relay fails to send the event to its message producer", env -> {
//
//                })
//                .Then("the relay catches an exception", env -> {
//
//                })
//                .And("the relay should be stopped", env -> {
//
//                })
                .When("the relay catches an exception while sending event to its message producer", env -> {

                    await().untilAsserted(() -> assertTrue(errContent.toString().contains("Fail to send message")));
                })
                .Then("the relay should be stopped", env -> {
                })
                .And("the value of the relay's checkpoint is not changed", env -> {
                })
                .Execute();

        System.setErr(originalErr);
    }

    @EzScenario(rule = RELAY_CONTINUATION)
    public void continue_the_relay() {
        final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        final PrintStream originalErr = System.err;
        System.setErr(new PrintStream(errContent));

        feature.newScenario()
                .Given("a MessageDb has 2 event", env -> {
                    MessageDbClient messageDbClient = Mockito.mock(FakeMessageDbClient.class);
                    Mockito.when(messageDbClient.findAllStreamFromPosition(1, 5)).thenReturn(List.of(
                            newUserRegisteredMessageData("user1", "user1@email.com"),
                            newUserRegisteredMessageData("user2", "user2@email.com")));
                    Mockito.when(messageDbClient.findAllStreamFromPosition(2, 5)).thenReturn(List.of(
                            newUserRegisteredMessageData("user2", "user2@email.com")));

                    env.put("messageDbClient", messageDbClient);
                })
                .And("an EzesCatchUpRelay has read the events from the MessageDb", env -> {
                    MessageProducer<DomainEventData> messageProducer = new TemporaryFailedMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withPollingInterval(500)
                            .withWindowSize(5);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    relayThread = Thread.ofVirtual().start(relay);
                    await().untilAsserted(() ->
                            Mockito.verify(env.get("messageDbClient", MessageDbClient.class),
                                    Mockito.times(1)).findAllStreamFromPosition(1, 5));

                })
                .When("the relay catches an exception while sending event to its message producer", env -> {
                    await().untilAsserted(() -> assertTrue(errContent.toString().contains("Fail to send message")));
                })
                .Then("the relay should be continue execution", env -> {
                    assertTrue(relayThread.isAlive());
                })
                .And("the value of the relay's checkpoint should be update to 3", env -> {
                    await().untilAsserted(() -> {
                        try {
                            assertEquals(3, EzesCatchUpRelay.Checkpoint.of(checkpointPath).value());
                        } catch (Exception e) {
                            fail(e);
                        }
                    });
                })
                .Execute();

        System.setErr(originalErr);
    }

    @EzScenario(rule = UPDATE_CHECKPOINT_FAILED)
    public void update_checkpoint_failed() {
        feature.newScenario()
                .Given("a MessageDb has two event", env -> {
                    MessageDbClient messageDbClient = new FakeMessageDbClient();
                    EventStoreData eventStoreData = new EventStoreData();
                    eventStoreData.setDomainEventDatas(List.of(
                            newUserRegisteredData("user1", "user1@email.com"),
                            newUserRegisteredData("user2", "user2@email.com")));

                    messageDbClient.saveDomainEvents(eventStoreData);

                    env.put("messageDbClient", messageDbClient);
                })
                .And("the checkpoint's value is 2", env -> {
                    createCheckpoint(checkpointPath, EzesCatchUpRelay.Checkpoint.of(2));
                })
                .When("a running EzesCatchUpRelay stops, causing an invalid checkpoint to be written", env -> {
                    MessageProducer<DomainEventData> messageProducer = new FakeMessageProducer();
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = EzesCatchUpRelay.RelayConfiguration
                            .of(env.get("messageDbClient", MessageDbClient.class),
                                    messageProducer,
                                    checkpointPath,
                                    new MessageDbToDomainEventDataConverter())
                            .withContinuationMode(ContinuationMode.TERMINATION)
                            .withBufferedWriterFactory(new FakeFilesBufferedWriterFactory());
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);

                    relayThread = Thread.ofVirtual().start(relay);

                    await().untilAsserted(() -> assertTrue(relayThread.isInterrupted()));
                    await().untilAsserted(() -> assertEquals(1, ((FakeMessageProducer) messageProducer).domainEventDatas.size()));
                    env.put("messageProducer", messageProducer);
                    env.put("relayConfiguration", relayConfiguration);
                })
                .Then("the relay should read the previous checkpoint when restarting the relay", env -> {
                    EzesCatchUpRelay.RelayConfiguration<DomainEventData> relayConfiguration = env.get("relayConfiguration", EzesCatchUpRelay.RelayConfiguration.class);
                    ActiveEventRelay<MessageData, DomainEventData, MessageProducer<DomainEventData>> relay =
                            new EzesCatchUpRelay<>(relayConfiguration);
                    relayThread = Thread.ofVirtual().start(relay);
                    await().untilAsserted(() -> {
                        FakeMessageProducer messageProducer = env.get("messageProducer", FakeMessageProducer.class);
                        assertEquals(2, messageProducer.domainEventDatas.size());
                        List<DomainEvent> domainEvents = DomainEventMapper.toDomain(messageProducer.domainEventDatas);
                        assertEquals(UserEvents.UserRegistered.class, domainEvents.get(1).getClass());
                        assertEquals("user2", ((UserEvents.UserRegistered) domainEvents.get(1)).username());
                        assertEquals(domainEvents.get(0), domainEvents.get(1));
                    });
                })
                .Execute();
    }

    private static void wait(int pollingInterval) {
        try {
            Thread.sleep(pollingInterval);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static void deleteFile(Path path) {
        if (!Files.exists(path)) return;
        try {
            Files.delete(path);
        } catch (FileSystemException e){
            // ignore the exception to file not exist
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createCheckpoint(Path checkpointPath, EzesCatchUpRelay.Checkpoint checkpoint) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(String.valueOf(checkpointPath)))) {
            writer.append(checkpoint.value().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static MessageData newUserRegisteredMessageData(String username, String email) {
        DomainEventData eventData = newUserRegisteredData(username, email);

        return new MessageData(
                eventData.id(),
                new JSONObject(new String(eventData.eventBody())).toString(),
                eventData.eventType(),
                "",
                Instant.now(),
                String.valueOf(new JSONObject(eventData.userMetadata())));
    }

    private static DomainEventData newUserRegisteredData(String username, String email) {
        return DomainEventMapper.toData(new UserEvents.UserRegistered(
                UUID.randomUUID().toString(),
                username,
                "pw",
                email,
                username,
                Role.User,
                true,
                UUID.randomUUID(),
                Instant.now()
        ));
    }

    static class FakeMessageProducer implements MessageProducer<DomainEventData> {
        List<DomainEventData> domainEventDatas = new ArrayList<>();

        @Override
        public void post(DomainEventData domainEventData) {
            domainEventDatas.add(domainEventData);
        }

        @Override
        public void close() throws IOException {

        }
    }

    static class ExceptionMessageProducer implements MessageProducer<DomainEventData> {

        @Override
        public void post(DomainEventData domainEventData) {
            throw new RuntimeException("Fail to send message");
        }

        @Override
        public void close() throws IOException {

        }
    }

    static class TemporaryFailedMessageProducer implements MessageProducer<DomainEventData> {
        private int count = 0;

        @Override
        public void post(DomainEventData domainEventData) {
            if (0 == count) {
                count++;
                throw new RuntimeException("Fail to send message");
            }
        }

        @Override
        public void close() throws IOException {

        }
    }

    static class FakeMessageDbClient implements MessageDbClient {
        List<MessageData> messageDatas = new ArrayList<>();

        @Override
        public long writeMessage(String id, String streamName, String type, String data, String metadata, Long expectedVersion) {
            return 0;
        }

        @Override
        public long writeMessage(String id, String streamName, String type, String data, String metadata) {
            return 0;
        }

        @Override
        public long getStreamVersion(String streamName) {
            return 0;
        }

        @Override
        public String getMessageStoreVersion() {
            return "";
        }

        @Override
        public List<MessageData> findByStreamName(String streamName) {
            return List.of();
        }

        @Override
        public List<MessageData> findByAggregateStreamName(String streamName) {
            return List.of();
        }

        @Override
        public List<MessageData> findByEventType(String eventType) {
            return List.of();
        }

        @Override
        public List<MessageData> findByCategory(String category) {
            return List.of();
        }

        @Override
        public List<MessageData> findAllStream() {
            return List.of();
        }

        @Override
        public List<MessageData> findByStreamNameAndEventTypeIn(String streamName, List<String> types) {
            return List.of();
        }

        @Override
        public long saveDomainEvents(EventStoreData eventStoreData) {
            eventStoreData.getDomainEventDatas().forEach(eventData ->
                    messageDatas.add(new MessageData(
                            eventData.id(),
                            new JSONObject(new String(eventData.eventBody())).toString(),
                            eventData.eventType(),
                            eventStoreData.getStreamName(),
                            Instant.now(),
                            String.valueOf(new JSONObject(eventData.userMetadata())))));

            return messageDatas.size();
        }

        @Override
        public void saveDomainEventsWithoutVersion(EventStoreData eventStoreData) {

        }

        @Override
        public List<MessageData> findAllStreamFromPosition(long nextPosition) {
            return messageDatas.subList((int) nextPosition - 1, messageDatas.size());
        }

        @Override
        public List<MessageData> findAllStreamFromPosition(long nextPosition, int windowSize) {
            int toPosition = (int) (windowSize + nextPosition - 1);
            if (messageDatas.size() < toPosition) {
                toPosition = messageDatas.size();
            }
            return messageDatas.subList((int) nextPosition - 1, toPosition);
        }
    }

    static class FakeFilesBufferedWriterFactory implements BufferedWriterFactory {
        @Override
        public BufferedWriter createWriter(Path path) throws IOException {
            return new FakeBufferedWriter(new FileWriter(String.valueOf(path)));
        }

        static class FakeBufferedWriter extends BufferedWriter {
            public FakeBufferedWriter(Writer out) {
                super(out);
            }

            @Override
            public void write(String str) throws IOException {
                super.write("invalid checkpoint");
                throw new IOException("invalid checkpoint");
            }
        }
    }
}
