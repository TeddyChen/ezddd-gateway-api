package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import io.github.resilience4j.core.IntervalFunction;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.data.adapter.in.relay.CallbackEventRelay;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbClientPool;
import tw.teddysoft.ezddd.message.broker.adapter.PostEventFailureException;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import static java.lang.String.format;
import static tw.teddysoft.ucontract.Contract.requireNotNull;

public class EsdbCatchUpRelay<Message>
        extends CallbackEventRelay<RecordedEvent, Message, MessageProducer<Message>> {

    private final EsdbClientPool esdbClientPool;
    private final Path checkpointPath;
    private Subscription subscription;
    private final String INCLUDE_EZKANBAN_EVENT_REGEX = "(\\w+Events\\$\\w+)";

    private final CountDownLatch cancellation = new CountDownLatch(1);
    private EventStoreDBClient client;

    public EsdbCatchUpRelay(EsdbClientPool esdbClientPool,
                            MessageProducer<Message> eventProducer,
                            Path checkpointPath,
                            Converter<RecordedEvent, Message> converter) {
        super(eventProducer, converter);

        requireNotNull("EsdbClientPool", esdbClientPool);
        requireNotNull("EventProducer", eventProducer);
        requireNotNull("CheckpointPath", checkpointPath);

        this.esdbClientPool = esdbClientPool;
        this.checkpointPath = checkpointPath;
    }

    @Override
    public void subscribe() {
        System.out.println("EsdbCatchUpRelay starts");

        try {
            SubscriptionListener listener = new CatchUpListener(3, 5000, 2);
            client = esdbClientPool.getClient();
            subscription = client.subscribeToAll(listener, getSubscribeToAllOptions()).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    private SubscribeToAllOptions getSubscribeToAllOptions() {
        Checkpoint checkpoint;
        SubscriptionFilter filter = SubscriptionFilter.newBuilder()
                .withEventTypeRegularExpression(INCLUDE_EZKANBAN_EVENT_REGEX)
                .build();

        SubscribeToAllOptions options = SubscribeToAllOptions.get()
                .filter(filter)
                .resolveLinkTos();

        if (Files.exists(checkpointPath)) {
            checkpoint = Checkpoint.of(checkpointPath);
            options = options.fromPosition(new Position(checkpoint.commitPosition(), checkpoint.preparePosition()));
        } else {
            options = options.fromStart();
        }

        return options;
    }

    @Override
    public void unsubscribe() {
        if (null != subscription) {
            subscription.stop();
            subscription = null;
            try {
                cancellation.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        if (null != client) {
            esdbClientPool.releaseClient(client);
        }

        try {
            eventProducer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    class CatchUpListener extends SubscriptionListener {

        private int maxAttempts;
        private int initialIntervalMillis;
        private int multiplier;

        public CatchUpListener(int maxAttempts, int initialIntervalMillis, int multiplier) {
            this.maxAttempts = maxAttempts;
            this.initialIntervalMillis = initialIntervalMillis;
            this.multiplier = multiplier;
        }

        @Override
        public void onEvent(Subscription subscription, ResolvedEvent event) {
            try {
                RecordedEvent record = event.getEvent();

                if (record.getEventType().startsWith("$")) {
//                    System.out.println("Got system event " + record.getEventType() + " ==========>");
                    return;
                }

                Message message = converter.convert(record);
//                System.out.println("onEvent, EventType =====>" + record.getEventType() + ", data = " + message);

                Retry.decorateRunnable(getRetry(), () -> eventProducer.post(message)).run();
                Checkpoint checkpoint = Checkpoint.of(event.getOriginalEvent().getPosition().getCommitUnsigned(), event.getOriginalEvent().getPosition().getPrepareUnsigned());
                updateCheckpoint(checkpoint);
            } catch (PostEventFailureException e) {
                e.printStackTrace();
                unsubscribe();
            } catch (IOException e) {
              //TODO: update checkpoint failure
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void updateCheckpoint(Checkpoint checkpoint) throws IOException{
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(String.valueOf(checkpointPath)))) {
                writer.append(checkpoint.toString());
            }
        }

        private Retry getRetry() {
            IntervalFunction intervalFn = IntervalFunction.ofExponentialBackoff(initialIntervalMillis, multiplier);
            RetryConfig config = RetryConfig.custom()
                    .maxAttempts(maxAttempts)
                    .intervalFunction(intervalFn)
                    .failAfterMaxAttempts(true)
                    .build();
            RetryRegistry registry = RetryRegistry.of(config);
            Retry retry = registry.retry("messageBus", config);
            return retry;
        }

        @Override
        public void onCancelled(Subscription subscription, Throwable exception) {
            cancellation.countDown();
            System.out.println("onCancelled  =====>" + subscription);
        }
    }

    record Checkpoint(Long commitPosition, Long preparePosition) {
        private static final String SPLIT = " ";

        public static Checkpoint of(Path path) {
            String checkpointStr = "";
            try {
                checkpointStr = Files.readString(path);
                String[] positions = checkpointStr.split(SPLIT);
                return new Checkpoint(Long.valueOf(positions[0]), Long.valueOf(positions[1]));
            } catch (NoSuchFileException e) {
                throw new CheckpointException("Checkpoint file not found.", e);
            } catch (IOException e) {
                throw new CheckpointException(e);
            } catch (NumberFormatException e) {
                throw new CheckpointException(format("Invalid Checkpoint format: %s", checkpointStr), e);
            }
        }

        public static Checkpoint of(long commitPosition, long preparePosition) {
            return new Checkpoint(commitPosition, preparePosition);
        }

        @Override
        public String toString() {
            return commitPosition + SPLIT + preparePosition;
        }
    }

    static class CheckpointException extends RuntimeException {
        public CheckpointException(Throwable cause) {
            super(cause);
        }

        public CheckpointException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}



