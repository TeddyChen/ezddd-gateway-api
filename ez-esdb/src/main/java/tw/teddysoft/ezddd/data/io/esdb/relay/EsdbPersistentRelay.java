package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.data.adapter.in.relay.CallbackEventRelay;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

public class EsdbPersistentRelay<Message>
        extends CallbackEventRelay<RecordedEvent, Message, MessageProducer<Message>> {

    private EventStoreDBPersistentSubscriptionsClient client;
    private final String groupName;
    private PersistentSubscription subscription;

    public EsdbPersistentRelay(EventStoreDBPersistentSubscriptionsClient client,
                               String groupName,
                               MessageProducer<Message> eventProducer,
                               Converter<RecordedEvent, Message> converter) {
        super(eventProducer, converter);

        requireNotNull("Client", client);
        requireNotNull("GroupName", groupName);
        requireNotNull("EventProducer", eventProducer);

        this.client = client;
        this.groupName = groupName;
    }

    @Override
    public void subscribe() {
        System.out.println("Esdb persistent listener starts");

        final CompletableFuture<Integer> result = new CompletableFuture<>();

        SubscribePersistentSubscriptionOptions connectOptions = SubscribePersistentSubscriptionOptions.get()
                .bufferSize(512);

        try {
            subscription = client.subscribeToAll(
                    groupName,
                    connectOptions,
                    new PersistentSubscriptionListener() {
                @Override
                public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent resolvedEvent) {

                    RecordedEvent record = resolvedEvent.getEvent();
//                    System.out.println("Persistent onEvent, EventType =====>" + record.getEventType());
                    // TODO: Need to guarantee domain event posted successfully before ack.
                    try{
                        eventProducer.post(converter.convert(record));
                        subscription.ack(resolvedEvent);
                    } catch (Exception e) {
                        e.printStackTrace();
//                        subscription.nack(NackAction.Retry, e.getMessage());
                    }
                }

                @Override
                public void onCancelled(PersistentSubscription subscription, Throwable exception) {
                    System.out.println("onCancelled");
                }
            }).get();

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unsubscribe() {
        if (null != subscription) {
            subscription.stop();
        }
        if (null != client) {
            client.shutdown();
        }
        try {
            eventProducer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

//    public void updatePersistentSubscription() {
//
//        PersistentSubscriptionToAllSettings updatedSettings = PersistentSubscriptionToAllSettings.builder()
//                .filter(filter)
////                .startFrom(0, 0)
//                .fromEnd()
//                .build();
//
//        UpdatePersistentSubscriptionToAllOptions options = UpdatePersistentSubscriptionToAllOptions.get()
//                .settings(updatedSettings);
//
//        try {
//            client.updateToAll(GROUP_NAME, options).get();
//        } catch (InterruptedException e) {
//            Thread.currentThread().interrupt();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//    }


}


