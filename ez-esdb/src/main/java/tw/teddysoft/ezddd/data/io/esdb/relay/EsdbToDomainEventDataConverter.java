package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.RecordedEvent;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;

import java.nio.charset.StandardCharsets;

public class EsdbToDomainEventDataConverter implements Converter<RecordedEvent, DomainEventData> {
    @Override
    public DomainEventData convert(RecordedEvent record) {
        return new DomainEventData(
                record.getEventId(),
                record.getEventType(),
                "application/json",
                record.getEventData(),
                "{}".getBytes(StandardCharsets.UTF_8));
    }
}
