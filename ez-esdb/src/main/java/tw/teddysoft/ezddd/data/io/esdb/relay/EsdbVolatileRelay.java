package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.data.adapter.in.relay.CallbackEventRelay;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbClientPool;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

public class EsdbVolatileRelay<Message>
        extends CallbackEventRelay<RecordedEvent, Message, MessageProducer<Message>> {

    private final EsdbClientPool esdbClientPool;
    private final DomainEventTypeMapper domainEventTypeMapper;
    private Subscription subscription;
    private final CountDownLatch cancellation = new CountDownLatch(1);
    private EventStoreDBClient client;

    public EsdbVolatileRelay(EsdbClientPool esdbClientPool,
                             DomainEventTypeMapper domainEventTypeMapper,
                             MessageProducer<Message> eventProducer,
                             Converter<RecordedEvent, Message> converter) {
        super(eventProducer, converter);

        requireNotNull("EsdbClientPool", esdbClientPool);
        requireNotNull("DomainEventTypeMapper", domainEventTypeMapper);
        requireNotNull("EventProducer", eventProducer);

        this.esdbClientPool = esdbClientPool;
        this.domainEventTypeMapper = domainEventTypeMapper;
    }

    @Override
    public void subscribe() {
        System.out.println("ESDB listener starts");

        SubscriptionListener listener = new SubscriptionListener() {
            @Override
            public void onEvent(Subscription subscription, ResolvedEvent event) {
                try {
                    RecordedEvent record = event.getEvent();
                    if (record.getEventType().startsWith("$")) {
//                        System.out.println("Got system event " + record.getEventType() + " ==========>");
                        return;
                    }

//                    System.out.println("onEvent, EventType =====>" + record.getEventType() + ", data = " + Arrays.toString(record.getEventData()));

                    eventProducer.post(converter.convert(record));
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(Subscription subscription, Throwable exception) {
                cancellation.countDown();
                System.out.println("onCancelled  =====>" + subscription);
            }

        };

        String excludeSystemEventsRegex = "^[^\\$]";
        SubscriptionFilter filter = SubscriptionFilter.newBuilder()
                .withEventTypeRegularExpression(excludeSystemEventsRegex)
                .build();

        SubscribeToAllOptions options = SubscribeToAllOptions.get()
                .filter(filter)
                .resolveLinkTos()
                .fromEnd();

        try {
            client = esdbClientPool.getClient();
            subscription = client.subscribeToAll(listener, options).get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unsubscribe() {
        if (null != subscription) {
            subscription.stop();
            try {
                cancellation.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        if(null != client) {
            esdbClientPool.releaseClient(client);
        }

        try {
            eventProducer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DomainEvent toDomain(String eventType, byte[] eventData) {
        try {
            Class<?> cls = domainEventTypeMapper.toClass(eventType);
            Object domainEvent = Json.readAs(
                    eventData, cls);
            return (DomainEvent) domainEvent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}



