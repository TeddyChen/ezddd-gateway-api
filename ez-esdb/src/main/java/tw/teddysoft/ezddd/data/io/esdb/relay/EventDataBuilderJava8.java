package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.EventData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import tw.teddysoft.ezddd.common.Json;

import java.util.UUID;

public class EventDataBuilderJava8 {

    private static final JsonMapper mapper = Json.mapper;

    private byte[] payload;
    private byte[] metadata;
    private String eventType;
    private boolean isJson;
    private UUID id;

    public static <A> EventDataBuilderJava8 json(String eventType, A payload) {
        EventDataBuilderJava8 self = new EventDataBuilderJava8();

        try {
            self.payload = mapper.writeValueAsBytes(payload);
            self.isJson = true;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        self.eventType = eventType;

        return self;
    }

    public EventData build() {
        UUID eventId = this.id == null ? UUID.randomUUID() : this.id;
        String contentType = this.isJson ? "application/json" : "application/octet-stream";
        return EventData.builderAsJson(eventId, this.eventType, this.payload).metadataAsBytes(this.metadata).build();
    }
}
