package tw.teddysoft.ezddd.data.io.esdb.store;

import com.eventstore.dbclient.*;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface EsdbClientPool {
    static WriteResult appendToStream(EsdbClientPool pool,
                                      String streamName,
                                      AppendToStreamOptions options,
                                      List<EventData> eventDatas) throws ExecutionException, InterruptedException {

        var client = pool.getClient();
        try {
            return client.appendToStream(streamName, options, eventDatas.iterator()).get();
        } finally {
            pool.releaseClient(client);
        }
    }

    static List<ResolvedEvent> getResolvedEvents(EsdbClientPool pool,
                                                 String streamName) throws InterruptedException, ExecutionException {

        ReadStreamOptions options = ReadStreamOptions.get()
                .resolveLinkTos()
                .forwards()
                .fromStart();

        var client = pool.getClient();

        try {
            return client.readStream(streamName, options)
                    .get()
                    .getEvents();
        } finally {
            pool.releaseClient(client);
        }
    }

    EventStoreDBClient getClient();

    boolean releaseClient(EventStoreDBClient eventStoreDBClient);

    int getSize();

    int getUsedSize();

    int getFreeSize();

    void dummyDestroy();

    void close();

    class NullEsdbMultiClientsPool implements EsdbClientPool{
        public NullEsdbMultiClientsPool() {
            super();
        }

        @Override
        public void close() {
            // do nothing
        }

        @Override
        public int getSize() {
            return 0;
        }

        @Override
        public int getUsedSize() {
            return 0;
        }

        @Override
        public int getFreeSize() {
            return 0;
        }

        @Override
        public void dummyDestroy() {

        }

        @Override
        public EventStoreDBClient getClient() {
            return null;
        }

        @Override
        public boolean releaseClient(EventStoreDBClient eventStoreDBClient) {
            return false;
        }
    }
}
