package tw.teddysoft.ezddd.data.io.esdb.store;

import com.eventstore.dbclient.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class EsdbMultiClientsPool implements EsdbClientPool {
    private final BlockingQueue<EventStoreDBClient> freeClients;
    private final List<EventStoreDBClient> usedClients;

    protected EsdbMultiClientsPool() {
        freeClients = null;
        usedClients = null;
    }

    public EsdbMultiClientsPool(String url, int poolSize) {
        freeClients = new ArrayBlockingQueue<>(poolSize);
        usedClients = new ArrayList<>();

        for (int i = 0; i < poolSize; i++) {
            EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(url);
            freeClients.add(EventStoreDBClient.create(settings));
        }
    }

    @Override
    public EventStoreDBClient getClient() {
        try {
            var client = freeClients.take();
            usedClients.add(client);
            return client;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return null;
    }

    @Override
    public boolean releaseClient(EventStoreDBClient eventStoreDBClient) {
        if (usedClients.remove(eventStoreDBClient)) {
            freeClients.add(eventStoreDBClient);
            return true;
        }
        return false;
    }

    @Override
    public int getSize() {
        return usedClients.size() + freeClients.size();
    }

    @Override
    public int getUsedSize() {
        return usedClients.size();
    }

    @Override
    public int getFreeSize() {
        return freeClients.size();
    }

    @Override
    public void dummyDestroy() {
        //for spring boot to destroy bean only
//        System.out.println();
    }

    @Override
    public void close() {
        freeClients.forEach(EventStoreDBClientBase::shutdown);
        usedClients.forEach(EventStoreDBClientBase::shutdown);
    }

}
