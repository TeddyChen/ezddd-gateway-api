package tw.teddysoft.ezddd.data.io.esdb.store;

import com.eventstore.dbclient.EventStoreDBClient;
import com.eventstore.dbclient.EventStoreDBClientSettings;
import com.eventstore.dbclient.EventStoreDBConnectionString;

public class EsdbSingleClientPool implements EsdbClientPool {

    private final EventStoreDBClient client;

    public EsdbSingleClientPool(String url) {
        EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(url);
        this.client = EventStoreDBClient.create(settings);
    }

    @Override
    public EventStoreDBClient getClient() {
        return client;
    }

    @Override
    public boolean releaseClient(EventStoreDBClient eventStoreDBClient) {
        return true;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public int getUsedSize() {
        return 0;
    }

    @Override
    public int getFreeSize() {
        return 1;
    }

    @Override
    public void dummyDestroy() {

    }

    @Override
    public void close() {
    }
}
