package tw.teddysoft.ezddd.data.io.esdb.store;

import com.eventstore.dbclient.*;
import tw.teddysoft.ezddd.data.adapter.repository.OptimisticLockingFailureException;
import tw.teddysoft.ezddd.data.adapter.repository.es.EventStore;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class EsdbStoreAdapter implements EventStore {

    private EsdbClientPool esdbClientPool;


    public EsdbStoreAdapter(EsdbClientPool esdbClientPool) {
        super();
        this.esdbClientPool = esdbClientPool;
    }

    @Override
    public void save(EventStoreData eventStoreData) {
        if (null == eventStoreData) {
            throw new RuntimeException("AggregateData cannot be null.");
        }
        List<EventData> eventDatas = new ArrayList<>();
        eventStoreData.getDomainEventDatas().stream().forEach(domainEventData -> {
            eventDatas.add(
                    EventData.builderAsJson(domainEventData.id(), domainEventData.eventType(), domainEventData.eventBody()).metadataAsBytes(domainEventData.userMetadata()).build());
        });

        try {
            AppendToStreamOptions options;

            if (-1 == eventStoreData.getOptimisticLockVersion()) {
                options = AppendToStreamOptions.get()
                        .expectedRevision(ExpectedRevision.any());
            } else {
                options = AppendToStreamOptions.get()
                        .expectedRevision(eventStoreData.getOptimisticLockVersion());
            }

            WriteResult writeResult = EsdbClientPool.appendToStream(esdbClientPool, eventStoreData.getStreamName(), options, eventDatas);

            eventStoreData.setVersion(writeResult.getNextExpectedRevision().toRawLong());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof WrongExpectedVersionException ex) {
                throw new OptimisticLockingFailureException(ex);
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    /*
     * Category name is event type
     * */
    @Override
    public Optional<EventStoreData> getEventsByType(String eventType) {
        return getEventsByStreamName(EventStore.EVENT_TYPE_PREFIX + eventType);
    }

    @Override
    public Optional<EventStoreData> getEventsByStreamName(String aggregateStreamName) {
        if (null == aggregateStreamName) {
            throw new IllegalArgumentException("AggregateStreamName cannot be null.");
        }

        List<ResolvedEvent> resolvedEvents = new ArrayList<>();
        try {
            resolvedEvents = EsdbClientPool.getResolvedEvents(esdbClientPool, aggregateStreamName);
        } catch (ExecutionException | InterruptedException e) {
            if (e.getCause() instanceof StreamNotFoundException) {
                return Optional.empty();
            } else {
                throw new RuntimeException(e);
            }
        }

        if (resolvedEvents.isEmpty())
            return Optional.empty();

        EventStoreData eventStoreData = new EventStoreData();
        resolvedEvents.stream().forEach(x -> {
            eventStoreData.getDomainEventDatas().add(toDomainEventData(x));
        });
        long revision = resolvedEvents.get(resolvedEvents.size() - 1).getEvent().getRevision();
        eventStoreData.setVersion(revision);
        eventStoreData.setStreamName(aggregateStreamName);

        return Optional.of(eventStoreData);
    }

    private DomainEventData toDomainEventData(ResolvedEvent resolvedEvent) {
        return new DomainEventData(
                resolvedEvent.getEvent().getEventId(),
                resolvedEvent.getEvent().getEventType(),
                resolvedEvent.getEvent().getContentType(),
                resolvedEvent.getEvent().getEventData(),
                resolvedEvent.getEvent().getUserMetadata());
    }
}
