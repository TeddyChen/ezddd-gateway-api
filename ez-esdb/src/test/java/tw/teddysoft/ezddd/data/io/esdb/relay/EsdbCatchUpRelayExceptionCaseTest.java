package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import org.junit.jupiter.api.*;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.data.io.esdb.EventStoreDbContainer;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbClientPool;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbSingleClientPool;
import tw.teddysoft.ezddd.message.broker.adapter.PostEventFailureException;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.EventBusProducer;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


@Testcontainers
public class EsdbCatchUpRelayExceptionCaseTest {

    @Container
    public static EventStoreDbContainer esdbContainer = new EventStoreDbContainer();

    private EventStoreDBClient client;
    private EsdbCatchUpRelay esdbCatchUpRelay;
    public static final Path checkpointPath = Path.of("C:\\Users\\ezKanban\\Desktop\\ezddd-gateway\\ez-esdb\\checkpoint.txt");
    public DomainEventTypeMapper domainEventTypeMapper;

    private EsdbClientPool esdbClientPool;
    private FakeDomainEventBus fakeDomainEventBus;
    private MessageProducer<DomainEventData> eventProducer;

    @BeforeEach
    public void setup() {
        domainEventTypeMapper = MyBoardEvents.mapper();
        DomainEventMapper.setMapper(domainEventTypeMapper);
        final String esdbUrl = new StringBuilder("esdb://")
                .append(esdbContainer.getHost())
                .append(":").append(esdbContainer.getMappedPort(2113))
                .append("?tls=false").toString();
        EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(esdbUrl);
        this.client = EventStoreDBClient.create(settings);
        esdbClientPool = new EsdbSingleClientPool(esdbUrl);

        fakeDomainEventBus = new FakeDomainEventBus();
        eventProducer = new EventBusProducer(fakeDomainEventBus);
        Converter<RecordedEvent, DomainEventData> converter = new EsdbToDomainEventDataConverter();
        esdbCatchUpRelay = new EsdbCatchUpRelay(esdbClientPool, eventProducer, checkpointPath, converter);
        esdbCatchUpRelay.subscribe();
    }

    @AfterEach
    public void teardown() {
        if (null != esdbCatchUpRelay)
            esdbCatchUpRelay.unsubscribe();
    }

    @AfterAll
    public static void afterAll() throws IOException {
        if (Files.exists(checkpointPath)){
            Files.delete(checkpointPath);
        }
    }

    @Test
    public void shutdown_relay_after_retried_fail() throws ExecutionException, InterruptedException, NoSuchFieldException, IllegalAccessException {
        String teamId = UUID.randomUUID().toString();
        String boardId = UUID.randomUUID().toString();

        MyBoardEvents.BoardCreated boardCreated = new
                MyBoardEvents.BoardCreated(
                teamId,
                boardId,
                "board name",
                UUID.randomUUID(),
                Instant.now());

        EventData boardCreatedData = EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(boardCreated),
                        boardCreated)
                .build();

        AppendToStreamOptions options = AppendToStreamOptions.get()
                .expectedRevision(ExpectedRevision.any());

        client.appendToStream("MyBoard-" + boardCreated.boardId(), options, boardCreatedData)
                .get();

        Field f = esdbCatchUpRelay.getClass().getDeclaredField("subscription"); //NoSuchFieldException
        f.setAccessible(true);
        await().timeout(30, TimeUnit.SECONDS).untilAsserted(() -> assertNull(f.get(esdbCatchUpRelay)));
    }

    @Test
    @Timeout(30)
    public void relay_listen_events_with_right_order() {


        Thread thread = new Thread(() -> {
            int count = 0;
            while(count < 25) {
                String teamId = UUID.randomUUID().toString();
                String boardId = UUID.randomUUID().toString();

                MyBoardEvents.BoardCreated boardCreated = new
                        MyBoardEvents.BoardCreated(
                        teamId,
                        boardId,
                        "board name",
                        UUID.randomUUID(),
                        Instant.now());

                EventData boardCreatedData = EventDataBuilderJava8.json(
                                domainEventTypeMapper.toMappingType(boardCreated),
                                boardCreated)
                        .build();

                AppendToStreamOptions options = AppendToStreamOptions.get()
                        .expectedRevision(ExpectedRevision.any());

                client.appendToStream("MyBoard-" + boardCreated.boardId(), options, boardCreatedData);
                count ++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        thread.run();
        assertEquals(1, fakeDomainEventBus.getDomainEvents().size());
    }

    class FakeDomainEventBus implements MessageBus<DomainEventData> {
        private Set<DomainEvent> domainEvents = new HashSet<>();

        public Set<DomainEvent> getDomainEvents() {
            return domainEvents;
        }

        @Override
        public void register(Reactor<DomainEventData> reactor) {

        }

        @Override
        public void unregister(Reactor<DomainEventData> reactor) {

        }

        @Override
        public void post(DomainEventData domainEventData) throws PostEventFailureException {
            domainEvents.add(DomainEventMapper.toDomain(domainEventData));
            throw new PostEventFailureException("Post event fail.");
        }
    }
}
