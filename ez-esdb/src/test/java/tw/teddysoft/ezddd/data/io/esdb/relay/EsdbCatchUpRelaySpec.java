package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.data.adapter.in.relay.CallbackEventRelay;
import tw.teddysoft.ezddd.data.io.esdb.EventStoreDbContainer;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbClientPool;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbSingleClientPool;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.BlockingMessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.EventBusProducer;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Testcontainers
public class EsdbCatchUpRelaySpec {

    public static Feature feature;

    private String ESDB_URL;

    @Container
    public static EventStoreDbContainer esdbContainer = new EventStoreDbContainer();
    @Container
    public KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.6.0"));

    private EventStoreDBClientSettings settings;
    private AppendToStreamOptions options;
    private EventStoreDBClient client;
    private EsdbClientPool esdbClientPool;
    public static final Path checkpointPath = Path.of("C:\\Users\\ezKanban\\Desktop\\ezddd-gateway\\ez-esdb\\checkpoint.txt");
    private MessageBus<DomainEventData> messageBus;
    private CallbackEventRelay relay;
    private DomainEventTypeMapper domainEventTypeMapper;
    private FakeListener listener;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Esdb Persistent Listener");
    }


    @BeforeEach
    public void setup() throws InterruptedException {
        String host = kafkaContainer.getBootstrapServers();
        Properties adminProperties = new Properties();
        adminProperties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        AdminClient adminClient = KafkaAdminClient.create(adminProperties);
        NewTopic topic = new NewTopic("test", 1, (short) 1);
        adminClient.createTopics(List.of(topic));
        adminClient.close();

        ESDB_URL = new StringBuilder("esdb://")
                .append(esdbContainer.getHost())
                .append(":").append(esdbContainer.getMappedPort(2113))
                .append("?tls=false").toString();
        domainEventTypeMapper = MyBoardEvents.mapper();
        DomainEventMapper.setMapper(domainEventTypeMapper);

        messageBus = new BlockingMessageBus<>();

        listener = new FakeListener();
        messageBus.register(listener);

        settings = EventStoreDBConnectionString.parseOrThrow(ESDB_URL);
        options = AppendToStreamOptions.get().expectedRevision(ExpectedRevision.any());
        client = EventStoreDBClient.create(settings);
        esdbClientPool = new EsdbSingleClientPool(ESDB_URL);

        Converter<RecordedEvent, DomainEventData> converter = new EsdbToDomainEventDataConverter();
        relay = new EsdbCatchUpRelay(esdbClientPool, new EventBusProducer(messageBus), checkpointPath, converter);
        relay.subscribe();
    }

    @AfterEach
    public void teardown() throws IOException {
        if (null != relay) {
            relay.unsubscribe();
        }
        listener.getDomainEvents().clear();
    }

    @AfterAll
    public static void afterAll() throws IOException {
        System.out.println("\n\n");
        System.out.println(feature.toString());
        if (Files.exists(checkpointPath)){
            Files.delete(checkpointPath);
        }
    }

    private void appendToStream(String boardId, EventData eventData) {
        try {
            client.appendToStream("MyBoard-" + boardId, options, eventData).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private EventData create_ezkanban_format_event_data(MyBoardEvents event) {
        return EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(event),
                        event)
                .build();
    }
    private EventData create_non_ezkanban_format_event_data(MyBoardEvents event, String nonEzKanbanFormat) {
        return EventDataBuilderJava8.json(
                        nonEzKanbanFormat,
                        event)
                .build();
    }

    public static String arrayToString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(a[i]));
            if (i == iMax)
                return b.append(']').toString();
            b.append(", ");
        }
    }

    @Nested
    class an_ezKanban_listener_basic_behavior {

        static Rule listenerBasicBehavior;

        @BeforeAll
        public static void beforeAll() {
            listenerBasicBehavior = feature.NewRule("An ezKanban listener basic behavior")
                    .description("""
                                In order to ensure eventual consistency among aggregates
                                As an ezKanban event listener
                                I want to receive correct domain events (and skip incorrect ones)
                            """);
        }

        @Test
        public void receives_ezKanban_format_events() {
            feature.newScenario().withRule(listenerBasicBehavior)
                    .Given("the persistent listener connects to esdb", env -> {
                    })
                    .When("a BoardEvents$BoardCreated event is appended to esdb", env -> {
                        String boardId = UUID.randomUUID().toString();
                        EventData eventData = create_ezkanban_format_event_data(MyBoardEvents.newBoardCreated(boardId));
                        appendToStream(boardId, eventData);
                    })
                    .And("published it to event bus", env -> {
                        await().untilAsserted(() -> assertEquals(1, listener.getDomainEvents().size()));
                        await().untilAsserted(() -> assertTrue(listener.getDomainEvents().get(0) instanceof MyBoardEvents.BoardCreated));
                    }).Execute();
        }

        @Test
        public void cannot_receive_non_ezKanban_format_events() {

            final String[] NON_EZKANBAN_FORMAT = {"$>", "ConfigProjection1", "BoardEvent$BoardCreated", "BoardEvents$"};
            feature.newScenario().withRule(listenerBasicBehavior)
                    .Given("the persistent listener connects to esdb", env -> {
                        relay.subscribe();
                    })
                    .When("four non-ezKanban format events " + arrayToString(NON_EZKANBAN_FORMAT) + " are appended to esdb", env -> {
                        String boardId = UUID.randomUUID().toString();
                        MyBoardEvents.BoardCreated boardCreated = MyBoardEvents.newBoardCreated(boardId);
                        for (String each : NON_EZKANBAN_FORMAT) {
                            appendToStream(boardId, create_non_ezkanban_format_event_data(boardCreated, each));
                        }
                    })
                    .Then("no event will be published", env -> {
                        await().untilAsserted(() -> assertEquals(0, listener.getDomainEvents().size()));
                    }).Execute();
        }
    }

    static class FakeListener implements Reactor<DomainEventData> {
        private List<DomainEvent> domainEvents = new ArrayList<>();

        public List<DomainEvent> getDomainEvents() {
            return domainEvents;
        }

        private void whenBoardCreated(MyBoardEvents.BoardCreated boardCreated) {
            domainEvents.add(boardCreated);
        }

        @Override
        public void execute(DomainEventData domainEventData) {
            switch (DomainEventMapper.toDomain(domainEventData)) {
                case MyBoardEvents.BoardCreated boardCreated -> whenBoardCreated(boardCreated);
                default -> {}
            }
        }
    }
}
