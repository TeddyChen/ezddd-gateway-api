package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.data.io.esdb.EventStoreDbContainer;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbClientPool;
import tw.teddysoft.ezddd.data.io.esdb.store.EsdbSingleClientPool;
import tw.teddysoft.ezddd.data.adapter.in.relay.CallbackEventRelay;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.BlockingMessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.EventBusProducer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.isA;


@Testcontainers
public class EsdbCatchUpRelayTest {

    @Container
    public static EventStoreDbContainer esdbContainer = new EventStoreDbContainer();

    private EventStoreDBClient client;
    private CallbackEventRelay esdbCatchUpRelay;
    public MessageBus<DomainEventData> messageBus;
    public static final Path checkpointPath = Path.of("C:\\Users\\ezKanban\\Desktop\\ezddd-gateway\\ez-esdb\\checkpoint.txt");
    public DomainEventTypeMapper domainEventTypeMapper;
    private FakeAllEventsListener.MockitoEventHandler mockitoEventHandler;

    private EsdbClientPool esdbClientPool;

    @BeforeEach
    public void setup() {
        mockitoEventHandler = Mockito.mock(FakeAllEventsListener.MockitoEventHandler.class);
        domainEventTypeMapper = MyBoardEvents.mapper();
        DomainEventMapper.setMapper(domainEventTypeMapper);
        messageBus = new BlockingMessageBus<>();
        messageBus.register(new FakeAllEventsListener(mockitoEventHandler));
        final String esdbUrl = new StringBuilder("esdb://")
                .append(esdbContainer.getHost())
                .append(":").append(esdbContainer.getMappedPort(2113))
                .append("?tls=false").toString();
        EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(esdbUrl);
        this.client = EventStoreDBClient.create(settings);
        esdbClientPool = new EsdbSingleClientPool(esdbUrl);
    }

    @AfterEach
    public void teardown() {
        if (null != esdbCatchUpRelay)
            esdbCatchUpRelay.unsubscribe();
    }

    @AfterAll
    public static void afterAll() throws IOException {
        if (Files.exists(checkpointPath)){
            Files.delete(checkpointPath);
        }
    }

    private void startRelay() {
        Converter<RecordedEvent, DomainEventData> converter = new EsdbToDomainEventDataConverter();
        esdbCatchUpRelay = new EsdbCatchUpRelay(esdbClientPool, new EventBusProducer(messageBus), checkpointPath, converter);
        esdbCatchUpRelay.subscribe();
    }

    @Test
    public void relay_should_catch_up_event_when_start() throws ExecutionException, InterruptedException {

        String teamId = UUID.randomUUID().toString();
        String boardId = UUID.randomUUID().toString();

        MyBoardEvents.BoardCreated boardCreated = new
                MyBoardEvents.BoardCreated(
                teamId,
                boardId,
                "board name",
                UUID.randomUUID(),
                Instant.now());

        EventData boardCreatedData = EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(boardCreated),
                        boardCreated)
                .build();

        AppendToStreamOptions options = AppendToStreamOptions.get()
                .expectedRevision(ExpectedRevision.any());

        client.appendToStream("MyBoard-" + boardCreated.boardId(), options, boardCreatedData)
                .get();

        MyBoardEvents.BoardRenamed boardRenamed = new
                MyBoardEvents.BoardRenamed(
                teamId,
                boardId,
                "new board name",
                UUID.randomUUID(),
                Instant.now());

        EventData boardRenamedData = EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(boardRenamed),
                        boardRenamed)
                .build();

        AppendToStreamOptions options2 = AppendToStreamOptions.get()
                .expectedRevision(ExpectedRevision.any());

        client.appendToStream("MyBoard-" + boardRenamed.boardId(), options2, boardRenamedData)
                .get();

        startRelay();

        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardCreated.class)));
        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardRenamed.class)));
    }

    @Test
    public void will_receive_a_event_from_esdb_and_publish_it_to_the_event_bus() throws ExecutionException, InterruptedException {

        startRelay();

        MyBoardEvents.BoardCreated boardCreated = new
                MyBoardEvents.BoardCreated(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                "board name",
                UUID.randomUUID(),
                Instant.now());

        EventData eventData = EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(boardCreated),
                        boardCreated)
                .build();

        AppendToStreamOptions options = AppendToStreamOptions.get()
                .expectedRevision(ExpectedRevision.noStream());

        client.appendToStream("MyBoard-" + boardCreated.boardId(), options, eventData)
                .get();

        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardCreated.class)));
    }

    @Test
    public void relay_should_read_from_checkpoint_when_restart() throws ExecutionException, InterruptedException {

        startRelay();

        String teamId = UUID.randomUUID().toString();
        String boardId = UUID.randomUUID().toString();
        MyBoardEvents.BoardCreated boardCreated = new
                MyBoardEvents.BoardCreated(
                teamId,
                boardId,
                "board name",
                UUID.randomUUID(),
                Instant.now());
        EventData boardCreatedData = EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(boardCreated),
                        boardCreated)
                .build();
        AppendToStreamOptions options = AppendToStreamOptions.get()
                .expectedRevision(ExpectedRevision.any());
        client.appendToStream("MyBoard-" + boardCreated.boardId(), options, boardCreatedData)
                .get();
        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardCreated.class)));

        esdbCatchUpRelay.unsubscribe();
        startRelay();

        MyBoardEvents.BoardRenamed boardRenamed = new
                MyBoardEvents.BoardRenamed(
                teamId,
                boardId,
                "new board name",
                UUID.randomUUID(),
                Instant.now());
        EventData boardRenamedData = EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(boardRenamed),
                        boardRenamed).build();
        AppendToStreamOptions options2 = AppendToStreamOptions.get()
                .expectedRevision(ExpectedRevision.any());
        client.appendToStream("MyBoard-" + boardRenamed.boardId(), options2, boardRenamedData).get();
        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardCreated.class)));
        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardRenamed.class)));
    }


    private static class FakeAllEventsListener implements Reactor<DomainEventData> {
        private final MockitoEventHandler mockitoEventHandler;

        public FakeAllEventsListener(MockitoEventHandler mockitoEventHandler) {
            this.mockitoEventHandler = mockitoEventHandler;
        }

        @Override
        public void execute(DomainEventData eventData) {
            this.mockitoEventHandler.execute(DomainEventMapper.toDomain(eventData));
        }

        private static class MockitoEventHandler {
            public void execute(DomainEvent event) {
                throw new UnsupportedOperationException();
            }
        }
    }

}
