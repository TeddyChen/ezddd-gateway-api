package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.in.interactor.Reactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.data.adapter.in.relay.CallbackEventRelay;
import tw.teddysoft.ezddd.data.io.esdb.EventStoreDbContainer;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.BlockingMessageBus;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.impl.EventBusProducer;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.isA;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Testcontainers
public class EsdbPersistentRelaySpec {

    public static Feature feature;

    private String ESDB_URL;

    @Container
    public static EventStoreDbContainer esdbContainer = new EventStoreDbContainer();

    private EventStoreDBClientSettings settings;
    private AppendToStreamOptions options;
    private EventStoreDBClient client;
    private EventStoreDBPersistentSubscriptionsClient persistentClient;
    private final static String GROUP_NAME = "EZKANBAN_MONO_MAIN";

    private CallbackEventRelay esdbPersistentRelay;
    private DomainEventTypeMapper domainEventTypeMapper;
    private MessageBus<DomainEventData> messageBus;
    private FakeAllEventsListener.MockitoEventHandler mockitoEventHandler;
    private SubscriptionFilter filter;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Esdb Persistent Listener");
    }


    @BeforeEach
    public void setup() throws InterruptedException {

        ESDB_URL = new StringBuilder("esdb://")
                .append(esdbContainer.getHost())
                .append(":").append(esdbContainer.getMappedPort(2113))
                .append("?tls=false").toString();
        mockitoEventHandler = Mockito.mock(FakeAllEventsListener.MockitoEventHandler.class);
        domainEventTypeMapper = MyBoardEvents.mapper();
        DomainEventMapper.setMapper(domainEventTypeMapper);
        messageBus = new BlockingMessageBus();
        messageBus.register(new FakeAllEventsListener(mockitoEventHandler));

        settings = EventStoreDBConnectionString.parseOrThrow(ESDB_URL);
        options = AppendToStreamOptions.get().expectedRevision(ExpectedRevision.any());
        client = EventStoreDBClient.create(settings);

        persistentClient = EventStoreDBPersistentSubscriptionsClient.create(settings);
        Converter<RecordedEvent, DomainEventData> converter = new EsdbToDomainEventDataConverter();
        esdbPersistentRelay = new EsdbPersistentRelay(
                persistentClient,
                GROUP_NAME,
                new EventBusProducer(messageBus),
                converter);

        String INCLUDE_EZKANBAN_EVENT_REGEX = "(\\w+Events\\$\\w+)";

        filter = SubscriptionFilter.newBuilder()
                .withEventTypeRegularExpression(INCLUDE_EZKANBAN_EVENT_REGEX)
                .build();

        try {
            persistentClient.createToAll(GROUP_NAME,
                    CreatePersistentSubscriptionToAllOptions.get()
                            .filter(filter)
                            .fromEnd()).get();

            System.out.println("group created");
        } catch (ExecutionException e) {
            if (e.getMessage().contains("ALREADY_EXISTS: Subscription group EZKANBAN_MONO_MAIN on stream $all exists.")) {
                // Ignore
            } else {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @AfterEach
    public void teardown() {
        if (null != esdbPersistentRelay) {
            esdbPersistentRelay.unsubscribe();
        }
        if (null != persistentClient) {
            persistentClient.deleteToAll(GROUP_NAME);
        }
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("\n\n");
        System.out.println(feature.toString());
    }


    @Nested
    class an_ezKanban_listener_basic_behavior {

        static Rule listenerBasicBehavior;

        @BeforeAll
        public static void beforeAll() {
            listenerBasicBehavior = feature.NewRule("An ezKanban listener basic behavior")
                    .description("""
                                In order to ensure eventual consistency among aggregates
                                As an ezKanban event listener
                                I want to receive correct domain events (and skip incorrect ones)
                            """);
        }

        @Test
        public void receives_ezKanban_format_events() {
            feature.newScenario().withRule(listenerBasicBehavior)
                    .Given("the persistent listener connects to esdb", env -> {
                        esdbPersistentRelay.subscribe();
                    })
                    .When("a BoardEvents$BoardCreated event is appended to esdb", env -> {
                        String boardId = UUID.randomUUID().toString();
                        EventData eventData = create_ezkanban_format_event_data(MyBoardEvents.newBoardCreated(boardId));
                        appendToStream(boardId, eventData);
                    })
                    .Then("the listener received the BoardEvents$BoardCreated event and published it to event bus", env -> {
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardCreated.class)));
                    }).Execute();
        }

        @Test
        public void cannot_receive_non_ezKanban_format_events() {

            final String[] NON_EZKANBAN_FORMAT = {"$>", "ConfigProjection1", "BoardEvent$BoardCreated", "BoardEvents$"};
            feature.newScenario().withRule(listenerBasicBehavior)
                    .Given("the persistent listener connects to esdb", env -> {
                        esdbPersistentRelay.subscribe();
                    })
                    .When("four non-ezKanban format events " + arrayToString(NON_EZKANBAN_FORMAT) + " are appended to esdb", env -> {
                        String boardId = UUID.randomUUID().toString();
                        MyBoardEvents.BoardCreated boardCreated = MyBoardEvents.newBoardCreated(boardId);
                        for (String each : NON_EZKANBAN_FORMAT) {
                            appendToStream(boardId, create_non_ezkanban_format_event_data(boardCreated, each));
                        }
                    })
                    .Then("the listener received nothing and no event will be published", env -> {
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.never()).execute(isA(MyBoardEvents.class)));
                    }).Execute();
        }
    }

    @Nested
    class a_esdb_persistent_listener {

        static Rule catchUpSystemState;

        @BeforeAll
        public static void beforeAll() {
            catchUpSystemState = feature.NewRule("Catch up system state")
                    .description("""
                            In order to ensure eventual consistency among aggregates
                            As a persistent listener
                            I want to catch up system state from the $All stream
                        """);
        }


        @Test
        public void ignore_previous_events_before_creating_group() {
            feature.newScenario().withRule(catchUpSystemState)
                    .Given("the all event stream has five events and no subscription exist", env -> {
                        esdbPersistentRelay.unsubscribe();
                        persistentClient.deleteToAll(GROUP_NAME);
                        String boardId = UUID.randomUUID().toString();
                        appendThreeEventsToEsdb(boardId);
                        appendTwoEventsToEsdb(boardId);
                    })
                    .When("the subscription created group", env -> {
                        mockitoEventHandler = Mockito.mock(FakeAllEventsListener.MockitoEventHandler.class);
                        messageBus.register(new FakeAllEventsListener(mockitoEventHandler));
                        try {
                            persistentClient.createToAll(GROUP_NAME,
                                    CreatePersistentSubscriptionToAllOptions.get()
                                            .filter(filter)
                                            .fromEnd()).get();
                        } catch (InterruptedException | ExecutionException e) {
                            throw new RuntimeException(e);
                        }
                        esdbPersistentRelay.subscribe();
                    })
                    .Then("the listener received nothing and no event will be published", env -> {
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.never()).execute(isA(MyBoardEvents.class)));
                    }).Execute();
        }

        @Test
        public void catch_up_events_from_last_known_position() {
            feature.newScenario().withRule(catchUpSystemState)
                    .Given("the all event stream has five events and two of them are unread", env -> {
                        String boardId = UUID.randomUUID().toString();
                        appendThreeEventsToEsdb(boardId);
                        readThreeEventsFromEsdb();
                        appendTwoEventsToEsdb(boardId);
                    })
                    .When("the relay restarts", env -> {
                        mockitoEventHandler = Mockito.mock(FakeAllEventsListener.MockitoEventHandler.class);
                        messageBus.register(new FakeAllEventsListener(mockitoEventHandler));
                        esdbPersistentRelay.subscribe();
                    })
                    .Then("it received the two unread events and published them to event bus", env -> {
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.never()).execute(isA(MyBoardEvents.BoardCreated.class)));
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.never()).execute(isA(MyBoardEvents.BoardRenamed.class)));
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardMoved.class)));
                        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardDeleted.class)));
                    }).Execute();
        }
    }

    private void appendToStream(String boardId, EventData eventData) {
        try {
            client.appendToStream("MyBoard-" + boardId, options, eventData).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void appendTwoEventsToEsdb(String boardId) {
        appendToStream(boardId, create_ezkanban_format_event_data(MyBoardEvents.newBoardMoved(boardId)));
        appendToStream(boardId, create_ezkanban_format_event_data(MyBoardEvents.newBoardDeleted(boardId)));
    }

    private void readThreeEventsFromEsdb() {
        esdbPersistentRelay.subscribe();
        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(1)).execute(isA(MyBoardEvents.BoardCreated.class)));
        await().untilAsserted(() -> Mockito.verify(mockitoEventHandler, Mockito.times(2)).execute(isA(MyBoardEvents.BoardRenamed.class)));
        esdbPersistentRelay.unsubscribe();
    }

    private void appendThreeEventsToEsdb(String boardId) {
        appendToStream(boardId, create_ezkanban_format_event_data(MyBoardEvents.newBoardCreated(boardId)));
        appendToStream(boardId, create_ezkanban_format_event_data(MyBoardEvents.newBoardRenamed(boardId)));
        appendToStream(boardId, create_ezkanban_format_event_data(MyBoardEvents.newBoardRenamed(boardId)));
    }

    private EventData create_ezkanban_format_event_data(MyBoardEvents event) {
        return EventDataBuilderJava8.json(
                        domainEventTypeMapper.toMappingType(event),
                        event)
                .build();
    }

    private EventData create_non_ezkanban_format_event_data(MyBoardEvents event, String nonEzKanbanFormat) {
        return EventDataBuilderJava8.json(
                        nonEzKanbanFormat,
                        event)
                .build();
    }

    public static String arrayToString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(a[i]));
            if (i == iMax)
                return b.append(']').toString();
            b.append(", ");
        }
    }

    private static class FakeAllEventsListener implements Reactor<DomainEventData> {
        private final MockitoEventHandler mockitoEventHandler;

        public FakeAllEventsListener(MockitoEventHandler mockitoEventHandler) {
            this.mockitoEventHandler = mockitoEventHandler;
        }

        @Override
        public void execute(DomainEventData eventData) {
            this.mockitoEventHandler.execute(DomainEventMapper.toDomain(eventData));
        }

        private static class MockitoEventHandler {
            public void execute(DomainEvent event) {
                throw new UnsupportedOperationException();
            }
        }
    }
}
