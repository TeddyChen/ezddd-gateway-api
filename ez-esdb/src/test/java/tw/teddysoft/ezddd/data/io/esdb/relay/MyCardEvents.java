package tw.teddysoft.ezddd.data.io.esdb.relay;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.entity.ValueObject;

import java.time.Instant;
import java.util.UUID;

public interface MyCardEvents extends DomainEvent {

    record BoardId(String id) implements ValueObject {

        public static BoardId valueOf(String id){
            return new BoardId(id);
        }

        public static BoardId valueOf(UUID id){
            return new BoardId(id.toString());
        }

        public static BoardId create(){
            return new BoardId(UUID.randomUUID().toString());
        }

        public String toString(){
            return id;
        }

    }

    BoardId boardId();
    String cardId();

    default String aggregateId(){
        return cardId();
    }

    ///////////////////////////////////////////////////////////////
    record CardCreated(
            BoardId boardId,
            String cardId,
            UUID id,
            Instant occurredOn
    ) implements MyCardEvents {}

    ///////////////////////////////////////////////////////////////

    static CardCreated newCardCreated(BoardId boardId, String cardId) {
        return new MyCardEvents.CardCreated(
                boardId,
                cardId,
                UUID.randomUUID(),
                Instant.now());
    }

    ///////////////////////////////////////////////////////////////


    class TypeMapper extends DomainEventTypeMapper.DefaultMapper {
        public static final String MAPPING_TYPE_PREFIX = "MyCardEvents$";
        public static final String CARD_CREATED = MAPPING_TYPE_PREFIX + "CardCreated";

        private static final DomainEventTypeMapper mapper;

        static {
            mapper = DomainEventTypeMapper.create();
            mapper.put(CARD_CREATED, CardCreated.class);
        }

        public static DomainEventTypeMapper getInstance(){
            return mapper;
        }
    }

    static DomainEventTypeMapper mapper(){
        return TypeMapper.getInstance();
    }

}
