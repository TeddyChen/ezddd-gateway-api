package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;

import java.util.concurrent.ExecutionException;

public class PersistentSubscriptions {

    public static final String STREAM_NAME = "$ce-Card";

    public static void createPersistentSubscription(EventStoreDBPersistentSubscriptionsClient client) throws ExecutionException, InterruptedException {
        // region create-persistent-subscription-to-stream
        client.createToStream(
                STREAM_NAME,
                "notify-workflow",
                CreatePersistentSubscriptionToStreamOptions.get()
                        .fromStart());
        // region create-persistent-subscription-to-stream
    }


    public static void connectToPersistentSubscriptionToStream(EventStoreDBPersistentSubscriptionsClient client) {

        client.subscribeToStream(
                STREAM_NAME,
                "notify-workflow",
                new PersistentSubscriptionListener() {
                    @Override
                    public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent event) {
//                        System.out.println("Received event"
//                                + event.getOriginalEvent().getRevision()
//                                + "@" + event.getOriginalEvent().getStreamId()
//                                + "---- " + event.getEvent().getEventType());

                        subscription.ack(event);
                    }
                    @Override
                    public void onCancelled(PersistentSubscription subscription, Throwable exception) {
                        System.out.println("Subscription is cancelled");
                    }
                });
        // region subscribe-to-persistent-subscription-to-stream
    }


    public static void connectToPersistentSubscriptionToAll(EventStoreDBPersistentSubscriptionsClient client) {

        String excludeSystemEventsRegex = "/^[^\\$].*/";
        SubscriptionFilter filter = SubscriptionFilter.newBuilder()
                .withEventTypeRegularExpression(excludeSystemEventsRegex)
                .build();

        CreatePersistentSubscriptionToAllOptions options =
                CreatePersistentSubscriptionToAllOptions.get()
                .fromStart();

        client.subscribeToAll(
                "notify-all",
                new PersistentSubscriptionListener() {
                    @Override
                    public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent event) {
//                        System.out.println("Received event from $all stream"
//                                + event.getOriginalEvent().getRevision()
//                                + "@" + event.getOriginalEvent().getStreamId()
//                                + "---- " + event.getEvent().getEventType());

                        subscription.ack(event);
                    }
                    @Override
                    public void onCancelled(PersistentSubscription subscription, Throwable exception) {
                        System.out.println("Subscription is cancelled");
                    }
                });
        // region subscribe-to-persistent-subscription-to-stream
    }

}
