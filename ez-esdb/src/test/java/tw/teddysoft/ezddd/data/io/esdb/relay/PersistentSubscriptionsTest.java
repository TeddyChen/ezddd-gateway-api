package tw.teddysoft.ezddd.data.io.esdb.relay;

import com.eventstore.dbclient.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Disabled
public class PersistentSubscriptionsTest {

    private String ESDB_URL;
    private EventStoreDBClientSettings settings;

    private DomainEventTypeMapper domainEventTypeMapper;

    class Foo {

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Foo foo1 = (Foo) o;
            return foo == foo1.foo;
        }

        @Override
        public int hashCode() {
            return Objects.hash(foo);
        }

        private boolean foo;

        public boolean isFoo() {
            return foo;
        }

        public void setFoo(boolean foo) {
            this.foo = foo;
        }
    }




    EventStoreDBPersistentSubscriptionsClient client;

    @BeforeEach
    public void setup(){
        settings = EventStoreDBConnectionString.parseOrThrow(ESDB_URL);
        domainEventTypeMapper = MyBoardEvents.mapper();
        client = EventStoreDBPersistentSubscriptionsClient.create(settings);
    }

    @AfterEach
    public void teardown() throws ExecutionException, InterruptedException {
        client.shutdown();
    }

    @Test
    public void testSubscribePersistentSubToAll() throws Throwable {
        EventStoreDBClient streamsClient = EventStoreDBClient.create(settings);

        String includeEzKanbanEventRegex = "(\\w+Events\\$\\w+)";
        SubscriptionFilter filter = SubscriptionFilter.newBuilder()
                .withEventTypeRegularExpression(includeEzKanbanEventRegex)
                .build();

        String streamName = "aStream-" + UUID.randomUUID().toString();
        try{
            client.createToAll("testGroup",
                    CreatePersistentSubscriptionToAllOptions.get()
                        .filter(filter)
                        .fromStart());
        } catch (Exception e){
            e.printStackTrace();
        }


        for(int n = 1; n < 11; n++){
            System.out.println(n);
            EventDataBuilder builder = EventData.builderAsJson("BoardEvents$CardCommitted" + String.valueOf(n), new Foo());
            streamsClient.appendToStream(streamName, builder.build()).get();
        }

        final CompletableFuture<Integer> result = new CompletableFuture<>();

        SubscribePersistentSubscriptionOptions connectOptions = SubscribePersistentSubscriptionOptions.get()
                .bufferSize(512);


        client.subscribeToAll("testGroup", connectOptions,new PersistentSubscriptionListener() {
            private int count = 0;

            @Override
            public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent resolvedEvent) {
                RecordedEvent event = resolvedEvent.getEvent();

                System.out.println("onEvent, EventType =====>" + event.getEventType());
                subscription.ack(resolvedEvent);

                ++this.count;

                if (this.count == 100) {
                    System.out.println("Got 10 events");
                    result.complete(this.count);
                    subscription.stop();
                }
            }

            @Override
            public void onCancelled(PersistentSubscription subscription, Throwable exception) {
                System.out.println("onCancelled");
                result.complete(count);
            }
        }).get();

    }


//    @Test
//    @Disabled
//    public void subscribe_persistent_subscription_to_all_with_filter() throws Throwable {
////        client.createToAll("notify-workflow",
////                PersistentSubscriptionToAllSettings.builder()
////                        .fromStart()
////                        .resolveLinkTos()
////                        .build()).get();
//        CreatePersistentSubscriptionToAllOptions updatedSettings = CreatePersistentSubscriptionToAllOptions.get()
//                .checkpointAfterInMs(5_000).
////                .revision(2);
//
//        UpdatePersistentSubscriptionOptions options = UpdatePersistentSubscriptionOptions.get()
//                .settings(updatedSettings);
//
//        client.update("aStream", "aGroupUpd", options)
//                .get();
//
//        SubscribePersistentSubscriptionOptions connectOptions = SubscribePersistentSubscriptionOptions.get()
//                .setBufferSize(32);
//
//        client.subscribeToAll("notify-workflow",
//                connectOptions,
//                new PersistentSubscriptionListener() {
//                    @Override
//                    public void onEvent(PersistentSubscription subscription, ResolvedEvent event) {
//                        System.out.println("Received event"
//                                + event.getOriginalEvent().getStreamRevision()
//                                + "@" + event.getOriginalEvent().getStreamId()
//                                + "---- " + event.getEvent().getEventType());
//
//                        RecordedEvent record = event.getEvent();
//                        record.getPosition();
//                        Pattern pattern = Pattern.compile("^[^\\$]");
//                        Matcher matcher = pattern.matcher(record.getEventType());
//                        if(matcher.find()) {
//                            System.out.println("Got system event " + record.getEventType() + " ==========>data= " + event.getEvent().getEventData());
//                            return;
//                        }
//
////                        if (record.getEventType().startsWith("$")){
////                            System.out.println("Got system event " + record.getEventType() + " ==========>data= " + event.getEvent().getEventData());
////                            return;
////                        }
//
//                        final DomainEvent domainEvent = toDomain(event.getEvent().getEventType(), event.getEvent().getEventData());
//                        System.out.println("onEvent, EventType =====>" + record.getEventType() + ", data = " + domainEvent.toString());
//
//                        subscription.ack(event);
//                    }
//
//                    @Override
//                    public void onError(PersistentSubscription subscription, Throwable throwable) {
//                        System.out.println("Subscription was dropped due to " + throwable.getMessage());
//                    }
//
//                    @Override
//                    public void onCancelled(PersistentSubscription subscription) {
//                        System.out.println("Subscription is cancelled");
//                    }
//                });
//
//        client.deleteToAll("notify-workflow")
//                .get();
//    }

//    @Test
//    @Disabled
//    public void subscribe_persistent_subscription_to_MyBoard() throws Throwable {
//        client.create(
//                "$ce-MyBoard",
//                "notify-workflow",
//                PersistentSubscriptionSettings.builder()
//                        .startFrom(1)
//                        .resolveLinkTos()
//                        .build()).get();
//
//        SubscribePersistentSubscriptionOptions connectOptions = SubscribePersistentSubscriptionOptions.get()
//                .setBufferSize(32);
//
//        client.subscribe(
//                "$ce-MyBoard",
//                "notify-workflow",
//                connectOptions,
//                new PersistentSubscriptionListener() {
//                    @Override
//                    public void onEvent(PersistentSubscription subscription, ResolvedEvent event) {
//                        System.out.println("Received event"
//                                + event.getOriginalEvent().getStreamRevision()
//                                + "@" + event.getOriginalEvent().getStreamId()
//                                + "---- " + event.getEvent().getEventType());
//                        RecordedEvent record = event.getEvent();
//                        if (record.getEventType().startsWith("$")){
//                            System.out.println("Got system event " + record.getEventType() + " ==========>data= " + event.getEvent().getEventData());
//                            return;
//                        }
//                        final DomainEvent domainEvent = toDomain(event.getEvent().getEventType(), event.getEvent().getEventData());
//                        System.out.println("onEvent, EventType =====>" + record.getEventType() + ", data = " + domainEvent.toString());
//
//                        subscription.ack(event);
//                    }
//
//                    @Override
//                    public void onError(PersistentSubscription subscription, Throwable throwable) {
//                        System.out.println("Subscription was dropped due to " + throwable.getMessage());
//                    }
//
//                    @Override
//                    public void onCancelled(PersistentSubscription subscription) {
//                        System.out.println("Subscription is cancelled");
//                    }
//                });
//
//    }

    public DomainEvent toDomain(String eventType, byte[] eventData) {
        try {
            Class<?> cls = domainEventTypeMapper.toClass(eventType);
            Object domainEvent = Json.readAs(
                    eventData, cls);
            return (DomainEvent) domainEvent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
