package tw.teddysoft.ezddd.data.io.esdb.store;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.data.io.esdb.EventStoreDbContainer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers
public class EsdbMultiClientsPoolTest {

    private String URL;
    private EsdbClientPool esdbClientPool;

    @Container
    public static EventStoreDbContainer esdbContainer = new EventStoreDbContainer();

    @BeforeEach
    public void setUp() {
        final String esdbUrl = new StringBuilder("esdb://")
                .append(esdbContainer.getHost())
                .append(":").append(esdbContainer.getMappedPort(2113))
                .append("?tls=false").toString();
        esdbClientPool = new EsdbMultiClientsPool(esdbUrl, 3);
    }

    @AfterEach
    public void tearDown() {
        esdbClientPool.close();
    }

    @Test
    public void normal_test() {

        assertEquals(3, esdbClientPool.getSize());
        var client1 = esdbClientPool.getClient();
        var client2 = esdbClientPool.getClient();
        var client3 = esdbClientPool.getClient();

        assertNotNull(client1);
        assertNotNull(client2);
        assertNotNull(client3);
        assertEquals(3, esdbClientPool.getSize());
        assertEquals(0, esdbClientPool.getFreeSize());
        assertEquals(3, esdbClientPool.getUsedSize());

        esdbClientPool.releaseClient(client1);
        assertEquals(1, esdbClientPool.getFreeSize());
        assertEquals(2, esdbClientPool.getUsedSize());

    }

}
