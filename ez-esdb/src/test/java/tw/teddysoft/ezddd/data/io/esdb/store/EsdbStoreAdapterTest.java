package tw.teddysoft.ezddd.data.io.esdb.store;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.data.adapter.repository.es.EventStore;
import tw.teddysoft.ezddd.data.adapter.repository.es.EsRepositoryPeerAdapter;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.out.repository.Repository;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EsRepository;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;
import tw.teddysoft.ezddd.data.io.esdb.EventStoreDbContainer;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
public class EsdbStoreAdapterTest {
    @Container
    public EventStoreDbContainer esdbContainer = new EventStoreDbContainer();

    @BeforeAll
    public static void beforeAll() {
        DomainEventMapper.setMapper(UserEvents.mapper());
    }
    @Test
    public void save_and_read() {
        String userId = UUID.randomUUID().toString();
        String username = "teddy";
        String password = "1234";
        String email = "teddy@gmail.com";
        String nickname = "Teddy";

        User user = new User(userId, username, password, email, nickname, Role.User, true);
        user.changeNickname("Ada");
        user.changeEmail("ada@gmail.com");

        final String esdbUrl = new StringBuilder("esdb://")
                .append(esdbContainer.getHost())
                .append(":").append(esdbContainer.getMappedPort(2113))
                .append("?tls=false").toString();

        EsdbClientPool esdbClientPool = new EsdbSingleClientPool(esdbUrl);
        EventStore eventStore = new EsdbStoreAdapter(esdbClientPool);
        RepositoryPeer<EventStoreData, String> repositoryPeer = new EsRepositoryPeerAdapter(eventStore);
        Repository<User, String> repository = new EsRepository<>(repositoryPeer, User.class, "User");

        assertEquals(3, user.getDomainEventSize());
        repository.save(user);
        assertEquals(0, user.getDomainEventSize());

        User modifiedUser = repository.findById(userId).get();

        assertEquals(username, modifiedUser.getUsername());
        assertEquals(password, modifiedUser.getPassword());
        assertEquals("Ada", modifiedUser.getNickname());
        assertEquals("ada@gmail.com", modifiedUser.getEmail());
    }
}
