package tw.teddysoft.ezddd.message.broker.adapter;

import com.google.common.eventbus.EventBus;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class InMemoryMessageBroker extends EventBus implements Runnable {
    private final BlockingQueue<DomainEventDto> queue;

    public InMemoryMessageBroker() {
        queue = new ArrayBlockingQueue<>(1024);
    }

    public synchronized void post(DomainEventDto domainEventDto) {
        try {
            queue.put(domainEventDto);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void postAll(List<DomainEventDto> domainEventDtos) {
        domainEventDtos.forEach(this::post);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                DomainEventDto domainEventDto = queue.take();
                super.post(domainEventDto);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
