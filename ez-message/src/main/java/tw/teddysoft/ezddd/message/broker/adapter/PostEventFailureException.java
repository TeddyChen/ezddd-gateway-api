package tw.teddysoft.ezddd.message.broker.adapter;

/**
 * {@code PostEventFailureException} is the subclass of {@code RuntimeException}.
 * When the MessageBus fails to post an event,
 * {@code PostEventFailureException} will be thrown.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 1.0.10
 */
public class PostEventFailureException extends RuntimeException{

    public PostEventFailureException(){
        super();
    }

    public PostEventFailureException(Exception e){
        super(e);
    }

    public PostEventFailureException(String message){
        super(message);
    }

    public PostEventFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}
