package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;

import java.io.Closeable;

abstract class AbstractMessageConsumer<Message> implements Closeable {
    protected final ExternalReactor<Message> externalReactor;

    protected AbstractMessageConsumer(ExternalReactor<Message> externalReactor) {
        this.externalReactor = externalReactor;
    }
}
