package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

public abstract class ActiveMessageConsumer extends AbstractMessageConsumer implements Runnable {

    protected ActiveMessageConsumer(ExternalReactor<DomainEventDto> externalReactor) {
        super(externalReactor);
    }
}
