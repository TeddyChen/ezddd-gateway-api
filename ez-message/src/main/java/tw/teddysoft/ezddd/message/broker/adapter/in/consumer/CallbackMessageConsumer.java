package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

public abstract class CallbackMessageConsumer extends AbstractMessageConsumer {

    protected CallbackMessageConsumer(ExternalReactor<DomainEventDto> externalReactor) {
        super(externalReactor);
    }

    public abstract void subscribe();
}
