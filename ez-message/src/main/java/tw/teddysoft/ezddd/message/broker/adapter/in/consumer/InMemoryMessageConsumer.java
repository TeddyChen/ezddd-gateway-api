package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import com.google.common.eventbus.Subscribe;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.message.broker.adapter.InMemoryMessageBroker;

import java.io.IOException;

public class InMemoryMessageConsumer extends CallbackMessageConsumer {

    public InMemoryMessageConsumer(InMemoryMessageBroker messageBroker, ExternalReactor<DomainEventDto> externalReactor) {
        super(externalReactor);
        messageBroker.register(this);
    }

    @Subscribe
    protected void when(DomainEventDto domainEventDto) {
        externalReactor.execute(domainEventDto);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void close() throws IOException {

    }
}
