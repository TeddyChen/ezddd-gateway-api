package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import edu.emory.mathcs.backport.java.util.Collections;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

public class KafkaMessageConsumer extends ActiveMessageConsumer {
    private final Consumer<String, String> consumer;
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public KafkaMessageConsumer(ExternalReactor<DomainEventDto> externalReactor, String host, String topic, String maxPollRecords, String groupId) {
        super(externalReactor);

        Properties consumerConfig = new Properties();
        consumerConfig.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        consumerConfig.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfig.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfig.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        consumerConfig.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerConfig.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);
        consumer = new KafkaConsumer<>(consumerConfig);
        consumer.subscribe(Collections.singletonList(topic));
    }

    public KafkaMessageConsumer(ExternalReactor<DomainEventDto> externalReactor,
                                String host,
                                String topic,
                                String groupId) {
        this(externalReactor, host, topic, "20", groupId);
    }

    @Override
    public void run() {
        try {
            while (!closed.get()) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, String> record : records) {
//                    System.out.println("Received message: " + record.value());
                    externalReactor.execute(Json.readValue(record.value(), DomainEventDto.class));
                }
                consumer.commitSync();
            }
        } catch (WakeupException e) {
            // Ignore exception if closing
            if (!closed.get()) throw e;
        } catch (Exception e) {
            // TODO: log the exception
            e.printStackTrace();
        } finally {
            consumer.close();
        }
    }

    @Override
    public void close() {
        closed.set(true);
        consumer.wakeup();
    }

}