package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import org.apache.pulsar.client.api.*;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

public class PulsarMessageConsumer extends ActiveMessageConsumer {

    private final PulsarClient pulsarClient;
    private final Consumer consumer;

    public PulsarMessageConsumer(ExternalReactor<DomainEventDto> externalReactor, String host, String topic) throws PulsarClientException {
        super(externalReactor);
        pulsarClient = PulsarClient.builder().serviceUrl(host).build();
        consumer = pulsarClient.newConsumer(Schema.AVRO(DomainEventDto.class)).topic(topic).subscriptionName("subscription").subscribe();
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Messages<DomainEventDto> messages = consumer.batchReceive();
                if (null == messages) continue;

                for (var message: messages) {
                    externalReactor.execute(message.getValue());
                    consumer.acknowledge(message);
                }
            } catch (PulsarClientException e) {
                if (e.getCause() instanceof InterruptedException || e instanceof PulsarClientException.AlreadyClosedException) {
                    consumer.closeAsync();
                    pulsarClient.closeAsync();
                    Thread.currentThread().interrupt();
                } else {
                    consumer.closeAsync();
                    pulsarClient.closeAsync();
                    throw new RuntimeException(e);
                }
            } catch (Exception e) {
                consumer.closeAsync();
                pulsarClient.closeAsync();
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void close() throws PulsarClientException {
        consumer.close();
        pulsarClient.close();
    }
}
