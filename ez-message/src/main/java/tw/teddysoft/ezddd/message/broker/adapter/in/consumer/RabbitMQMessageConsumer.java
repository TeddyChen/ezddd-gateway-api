package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQMessageConsumer extends CallbackMessageConsumer {

    private final Channel consumerChannel;
    private Connection connection;
    private final String queueName;

    public RabbitMQMessageConsumer(ExternalReactor<DomainEventDto> externalReactor, String host, int port, String queueName) {
        super(externalReactor);
        this.queueName = queueName;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        try {
            connection = factory.newConnection();
            consumerChannel = connection.createChannel();
            consumerChannel.queueDeclare(this.queueName, false, false, false, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void subscribe() {
        try {
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                try {
                    DomainEventDto domainEventDto = Json.readAs(delivery.getBody(), DomainEventDto.class);
                    externalReactor.execute(domainEventDto);
                    consumerChannel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (Exception e) {
//                    e.printStackTrace();
                    throw new RuntimeException(e);
                }

            };
            consumerChannel.basicConsume(queueName, false, deliverCallback, consumerTag -> {
            });
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws IOException {
        if (null == consumerChannel || !consumerChannel.isOpen())
            return;
        try {
            consumerChannel.close();
            connection.close();
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}
