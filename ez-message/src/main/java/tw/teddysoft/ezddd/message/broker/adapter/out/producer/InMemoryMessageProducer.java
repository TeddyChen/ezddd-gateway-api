package tw.teddysoft.ezddd.message.broker.adapter.out.producer;

import tw.teddysoft.ezddd.message.broker.adapter.PostEventFailureException;
import tw.teddysoft.ezddd.message.broker.adapter.InMemoryMessageBroker;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;

public class InMemoryMessageProducer implements MessageProducer<DomainEventData> {

    private final InMemoryMessageBroker messageBroker;

    public InMemoryMessageProducer(InMemoryMessageBroker messageBroker) {
        this.messageBroker = messageBroker;
    }

    @Override
    public void post(DomainEventData domainEventData) throws PostEventFailureException {
        messageBroker.post(domainEventData);
    }

    @Override
    public void close() throws IOException {

    }
}
