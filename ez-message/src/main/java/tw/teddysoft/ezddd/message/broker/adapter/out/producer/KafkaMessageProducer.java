package tw.teddysoft.ezddd.message.broker.adapter.out.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.message.broker.adapter.PostEventFailureException;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class KafkaMessageProducer<Message> implements MessageProducer<Message> {
    private final KafkaProducer<String, String> producer;
    private final String topic;

    public KafkaMessageProducer(String host, String topic) {
        this.topic = topic;
        Properties config = new Properties();
        config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producer = new KafkaProducer<>(config);
    }

    @Override
    public void post(Message message) {
        try {
            producer.send(new ProducerRecord<>(topic, Json.asString(message))).get();
        } catch (InterruptedException e) {
            throw new PostEventFailureException(e);
        } catch (ExecutionException e) {
            throw new PostEventFailureException(e);
        }
    }

    @Override
    public void close() throws IOException {
        producer.close();
    }
}
