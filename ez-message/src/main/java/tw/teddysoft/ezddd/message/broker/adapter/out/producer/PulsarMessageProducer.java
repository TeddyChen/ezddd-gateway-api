package tw.teddysoft.ezddd.message.broker.adapter.out.producer;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.message.broker.adapter.PostEventFailureException;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;

public class PulsarMessageProducer implements MessageProducer<DomainEventDto> {

    private final Producer<DomainEventDto> producer;
    private final PulsarClient pulsarClient;

    public PulsarMessageProducer(String host, String topic) throws PulsarClientException {
        pulsarClient = PulsarClient.builder().serviceUrl(host).build();
        producer = pulsarClient.newProducer(Schema.AVRO(DomainEventDto.class)).topic(topic).create();
    }

    @Override
    public void post(DomainEventDto domainEventDto) throws PostEventFailureException {
        try {
            producer.newMessage().value(domainEventDto).send();
        } catch (PulsarClientException e) {
            throw new PostEventFailureException(e);
        }
    }

    @Override
    public void close() throws IOException {
        producer.close();
        pulsarClient.close();
    }
}

