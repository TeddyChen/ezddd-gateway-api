package tw.teddysoft.ezddd.message.broker.adapter.out.producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.broker.adapter.PostEventFailureException;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQMessageProducer implements MessageProducer<DomainEventData> {

    private final Channel producerChannel;
    private Connection connection;
    private final String queueName;

    public RabbitMQMessageProducer(String host, int port, String queueName) {
        this.queueName = queueName;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        try {
            connection = factory.newConnection();
            producerChannel = connection.createChannel();
            producerChannel.queueDeclare(this.queueName, false, false, false, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void post(DomainEventData domainEventData) throws PostEventFailureException {
        try {
            producerChannel.basicPublish("", queueName, null, Json.asString(domainEventData).getBytes());
        } catch (IOException e) {
            throw new PostEventFailureException(e);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            producerChannel.close();
            connection.close();
        } catch (TimeoutException e) {
            throw new IOException(e);
        }
    }
}
