package tw.teddysoft.ezddd.message.reactor.adapter.in;


public abstract class CoRExternalReactor<Message> implements ExternalReactor<Message> {
    protected CoRExternalReactor<Message> next;

    public CoRExternalReactor() {
        this.next = null;
    }

    public void setNext(CoRExternalReactor<Message> next) {
        this.next = next;
    }

    public boolean hasNext(){
        return null != next;
    }
}
