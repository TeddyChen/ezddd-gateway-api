package tw.teddysoft.ezddd.message.reactor.adapter.in;

public interface ExternalReactor<Message> {
    void execute(Message message);
}
