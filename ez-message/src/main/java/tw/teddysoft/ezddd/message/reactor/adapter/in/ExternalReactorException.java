package tw.teddysoft.ezddd.message.reactor.adapter.in;

public class ExternalReactorException extends RuntimeException {

    public ExternalReactorException(Exception e) {
        super(e);
    }

    public ExternalReactorException(String e) {
        super(e);
    }
}
