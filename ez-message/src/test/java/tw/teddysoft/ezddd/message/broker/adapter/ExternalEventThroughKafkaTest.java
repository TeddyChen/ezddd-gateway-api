package tw.teddysoft.ezddd.message.broker.adapter;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.message.broker.adapter.in.consumer.ActiveMessageConsumer;
import tw.teddysoft.ezddd.message.broker.adapter.in.consumer.KafkaMessageConsumer;
import tw.teddysoft.ezddd.message.broker.adapter.out.producer.KafkaMessageProducer;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
public class ExternalEventThroughKafkaTest {
    private static Feature feature;
    private static Rule relayRule;

    @Container
    public KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.6.0"));

    private final String TOPIC = "test";
    private String host;
    private ActiveMessageConsumer messageController;
    private ExternalReactor<DomainEventDto> externalReactor;
    private MessageProducer<DomainEventDto> messageProducer;
    private Thread messageCtrlThread;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Kafka Message Controller");
        relayRule = feature.NewRule("A Kafka message controller basic behavior")
                .description("""
                                In order to ensure eventual consistency among bounded context
                                As a kafka message controller
                                I want to receive correct remote domain events and post them to ACL handler
                            """);
    }

    @BeforeEach
    public void setup() {

        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();
        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(typeMapper);

        host = kafkaContainer.getBootstrapServers();
        Properties adminProperties = new Properties();
        adminProperties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        AdminClient adminClient = KafkaAdminClient.create(adminProperties);
        NewTopic topic = new NewTopic(TOPIC, 1, (short) 1);
        adminClient.createTopics(List.of(topic));
        adminClient.close();

    }

    @AfterEach
    public void tearDown() throws IOException {
        if(null != messageProducer) {
            messageProducer.close();
        }
        if(null != messageCtrlThread) {
            messageCtrlThread.interrupt();
            messageController.close();
        }
    }

    @Test
    public void acl_handler_should_receive_a_remote_domain_event_posted_from_message_producer() {

        String teamId = "teamId";
        String boardId = "boardId";
        String boardName = "boardName";
        UUID id = UUID.randomUUID();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker Kafka with a topic 'test' already started", env -> {
                })
                .And("an ACL handler is created", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                })
                .And("an message controller connects to the Kafka and subscribe to the topic 'test' to forward messages to the ACL handler", env -> {
                    messageController = new KafkaMessageConsumer(externalReactor, host, TOPIC, "1");
                    messageCtrlThread = new Thread(messageController);
                    messageCtrlThread.start();
                })
                .And("a message producer connect to the Kafka and subscribe to the topic 'test'", env -> {
                    messageProducer = new KafkaMessageProducer(host, "test");
                })
                .When("the message producer post a remote domain event to Kafka", env -> {
                    DomainEvent domainEvent = new MyBoardEvents.BoardCreated(
                            teamId,
                            boardId,
                            boardName,
                            id,
                            Instant.now());

                    messageProducer.post(
                                    new DomainEventDto(
                                            domainEvent,
                                            "Board",
                                            Instant.now()));

                })
                .Then("the message controller should receive a remote domain event from Kafka", env -> {
                })
                .And("the message controller should post the remote domain event to the ACL handler", env -> {
                })
                .And("the ACL handler should receive the remote domain event", env -> {
                    await().untilAsserted(() -> assertEquals(1, domainEventDtos.size()));
                    DomainEventDto domainEvent = domainEventDtos.get(0);

                    JsonNode jsonNode = Json.readTree(domainEvent.getJsonEvent());
                    assertEquals(teamId, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName, jsonNode.at("/boardName").textValue());
                    assertEquals(id, UUID.fromString(jsonNode.at("/id").textValue()));

                }).Execute();
    }

    class FakeExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private List<DomainEventDto> executedDomainEventDtos;

        public FakeExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
            executedDomainEventDtos = new ArrayList<>();
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (!this.domainEventDtos.stream().anyMatch(x->x.id().equals(domainEventDto.id()))) {
                this.domainEventDtos.add(domainEventDto);
                executedDomainEventDtos.add(domainEventDto);
            }
        }
    }
}
