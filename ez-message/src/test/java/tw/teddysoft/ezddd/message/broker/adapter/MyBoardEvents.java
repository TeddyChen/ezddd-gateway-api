package tw.teddysoft.ezddd.message.broker.adapter;

import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;

import java.time.Instant;
import java.util.UUID;

public interface MyBoardEvents extends DomainEvent {

    String boardId();

    default String aggregateId(){
        return boardId();
    }

    ///////////////////////////////////////////////////////////////
    record BoardCreated(
            String teamId,
            String boardId,
            String boardName,
            UUID id,
            Instant occurredOn
    ) implements MyBoardEvents  {}

    ///////////////////////////////////////////////////////////////
    record BoardRenamed(
            String teamId,
            String boardId,
            String boardName,
            UUID id,
            Instant occurredOn
    ) implements MyBoardEvents {}

    ///////////////////////////////////////////////////////////////
    record BoardDeleted(
            String teamId,
            String boardId,
            UUID id,
            Instant occurredOn
    ) implements MyBoardEvents {}

    ///////////////////////////////////////////////////////////////
    record BoardMoved(
            String teamId,
            String boardId,
            UUID id,
            Instant occurredOn
    ) implements MyBoardEvents {}

    static BoardCreated newBoardCreated(String boardId) {
        return new BoardCreated(
                UUID.randomUUID().toString(),
                boardId,
                "board name",
                UUID.randomUUID(),
                Instant.now());
    }

    static BoardRenamed newBoardRenamed(String boardId) {
        return new BoardRenamed(
                UUID.randomUUID().toString(),
                boardId,
                "new board name",
                UUID.randomUUID(),
                Instant.now());
    }

    static BoardDeleted newBoardDeleted(String boardId) {
        return new BoardDeleted(
                UUID.randomUUID().toString(),
                boardId,
                UUID.randomUUID(),
                Instant.now());
    }

    static BoardMoved newBoardMoved(String boardId) {
        return new BoardMoved(
                UUID.randomUUID().toString(),
                boardId,
                UUID.randomUUID(),
                Instant.now());
    }
    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////


    class TypeMapper extends DomainEventTypeMapper.DefaultMapper {
        public static final String MAPPING_TYPE_PREFIX = "MyBoardEvents$";
        public static final String BOARD_CREATED = MAPPING_TYPE_PREFIX + "BoardCreated";
        public static final String BOARD_RENAMED = MAPPING_TYPE_PREFIX + "BoardRenamed";
        public static final String BOARD_DELETED = MAPPING_TYPE_PREFIX + "BoardDeleted";
        public static final String BOARD_MOVED = MAPPING_TYPE_PREFIX + "BoardMoved";

        private static final DomainEventTypeMapper mapper;

        static {
            mapper = DomainEventTypeMapper.create();
            mapper.put(BOARD_CREATED, BoardCreated.class);
            mapper.put(BOARD_RENAMED, BoardRenamed.class);
            mapper.put(BOARD_DELETED, BoardDeleted.class);
            mapper.put(BOARD_MOVED, BoardMoved.class);
        }

        public static DomainEventTypeMapper getInstance(){
            return mapper;
        }
    }

    static DomainEventTypeMapper mapper(){
        return TypeMapper.getInstance();
    }

}
