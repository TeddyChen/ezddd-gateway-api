package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;//package tw.teddysoft.ezddd.eventbroker.adapter.relay;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;
import tw.teddysoft.ezddd.message.broker.adapter.InMemoryMessageBroker;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class InMemoryMessageConsumerTest {
    private static Feature feature;
    private static Rule relayRule;

    private CallbackMessageConsumer messageConsumer;
    private ExternalReactor<DomainEventDto> externalReactor;
    private Thread brokerThread;
    private InMemoryMessageBroker messageBroker = new InMemoryMessageBroker();

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("In-Memory Message Consumer");
        relayRule = feature.NewRule("An in-memory message consumer basic behavior")
                .description("""
                            In order to ensure eventual consistency among bounded context
                            As an in-memory message consumer
                            I want to receive correct remote domain events and post them to acl handler
                        """);
    }

    @BeforeEach
    public void setup() {
        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();
        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(typeMapper);
    }

    @AfterEach
    public void tearDown() throws IOException {
        if (null != brokerThread) {
            brokerThread.interrupt();
            messageConsumer.close();
        }
    }

    @EzScenario
    public void should_forward_an_event_from_in_memory_message_broker_to_event_bus() {
        String teamId = "teamId";
        String boardId = "boardId";
        String boardName = "boardName";
        UUID id = UUID.randomUUID();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("an acl handler is created", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                })
                .And("an message consumer connects to the in-memory message broker to forward messages to the acl handler", env -> {
                    messageConsumer = new InMemoryMessageConsumer(messageBroker, externalReactor);
                })
                .And("the in-memory message broker is started", env -> {
                    brokerThread = new Thread(messageBroker);
                    brokerThread.start();
                })
                .When("a remote domain event is sent to in-memory message broker", env -> {
                    DomainEvent domainEvent = new MyBoardEvents.BoardCreated(
                            teamId,
                            boardId,
                            boardName,
                            id,
                            Instant.now());

                    messageBroker.post(new DomainEventDto(domainEvent,
                            "Board",
                            Instant.now()));
                })
                .Then("the message consumer should receive a remote domain event from in-memory message broker", env -> {
                    await().untilAsserted(() -> assertEquals(1, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);

                    assertEquals("BoardCreated", event.getEventSimpleName());
                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName, jsonNode.at("/boardName").textValue());
                    assertEquals(id, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    class FakeExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private List<DomainEventDto> executedDomainEventDtos;

        public FakeExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
            executedDomainEventDtos = new ArrayList<>();
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (!this.domainEventDtos.stream().anyMatch(x->x.id().equals(domainEventDto.id()))) {
                this.domainEventDtos.add(domainEventDto);
                executedDomainEventDtos.add(domainEventDto);
            }
        }
    }
}
