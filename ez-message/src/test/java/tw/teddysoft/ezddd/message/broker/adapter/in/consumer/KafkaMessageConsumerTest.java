package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Testcontainers
public class KafkaMessageConsumerTest {
    private static Feature feature;
    private static Rule relayRule;

    @Container
    public KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.6.0"));

    private final String TOPIC = "KafkaMessageConsumerTest";
    private String host;
    private ActiveMessageConsumer messageConsumer;
    private ExternalReactor<DomainEventDto> externalReactor;
    private Thread consumerThread;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Kafka Message Consumer");
        relayRule = feature.NewRule("A Kafka consumer basic behavior")
                .description("""
                            In order to ensure eventual consistency among bounded context
                            As a kafka consumer
                            I want to receive correct remote domain events and post them to acl consumer
                        """);
    }

    @BeforeEach
    public void setup() {

        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();

        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(typeMapper);

        host = kafkaContainer.getBootstrapServers();
        Properties adminProperties = new Properties();
        adminProperties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        AdminClient adminClient = KafkaAdminClient.create(adminProperties);
        NewTopic topic = new NewTopic(TOPIC, 1, (short) 1);
        adminClient.createTopics(List.of(topic));
        adminClient.close();
    }

    @AfterEach
    public void tearDown() throws IOException {
        if (null != consumerThread) {
            consumerThread.interrupt();
            messageConsumer.close();
        }
    }

    @EzScenario
    public void should_forward_an_event_from_Kafka_to_acl_consumer() {
        String teamId = "teamId";
        String boardId = "boardId";
        String boardName = "boardName";
        UUID id = UUID.randomUUID();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker Kafka with a topic 'test' already started", env -> {
                })
                .And("an acl consumer is created", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                })
                .And("a consumer connects to the Kafka and subscribe to the topic 'test' to forward messages to the acl consumer", env -> {
                    messageConsumer = new KafkaMessageConsumer(externalReactor, host, TOPIC, "1", "1");
                    consumerThread = new Thread(messageConsumer);
                    consumerThread.start();
                })
                .When("a remote domain event is sent to Kafka", env -> {
                    DomainEvent domainEvent = new MyBoardEvents.BoardCreated(
                            teamId,
                            boardId,
                            boardName,
                            id,
                            Instant.now());

                    Properties config = new Properties();
                    config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
                    config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                    config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                    KafkaProducer<String, String> producer = new KafkaProducer<>(config);

                    try {
                        producer.send(new ProducerRecord<>(TOPIC, Json.asString(
                                new DomainEventDto(
                                        domainEvent,
                                        "Board",
                                        Instant.now())))).get();
                    } catch (InterruptedException | ExecutionException e) {
                        throw new RuntimeException(e);
                    } finally {
                        producer.close();
                    }
                })
                .Then("the acl consumer should receive a remote domain event from Kafka", env -> {
                    await().untilAsserted(() -> assertEquals(1, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);
                    assertEquals("BoardCreated", event.getEventSimpleName());

                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName, jsonNode.at("/boardName").textValue());
                    assertEquals(id, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    @EzScenario
    public void should_resend_an_event_after_crash() {
        String teamId1 = "teamId1";
        String teamId2 = "teamId2";
        String teamId3 = "teamId3";
        String boardId1 = "boardId1";
        String boardId2 = "boardId2";
        String boardId3 = "boardId3";
        String boardName1 = "boardName1";
        String boardName2 = "boardName2";
        String boardName3 = "boardName3";
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        List<DomainEventDto> receivedDomainEventDtos = new ArrayList<>();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker Kafka with a topic 'test' already started", env -> {
                })
                .And("an incorrect acl consumer", env -> {
                    externalReactor = new IncorrectExternalReactor(receivedDomainEventDtos);
                })
                .And("an consumer connects to the Kafka and subscribe to the topic 'test' to forward messages to the acl consumer", env -> {
                    messageConsumer = new KafkaMessageConsumer(externalReactor, host, TOPIC, "1", "1");
                    consumerThread = new Thread(messageConsumer);
                    consumerThread.start();
                })
                .And("three remote domain event are sent to Kafka", env -> {
                    DomainEvent domainEvent1 = new MyBoardEvents.BoardCreated(
                            teamId1,
                            boardId1,
                            boardName1,
                            id1,
                            Instant.now());
                    DomainEvent domainEvent2 = new MyBoardEvents.BoardCreated(
                            teamId2,
                            boardId2,
                            boardName2,
                            id2,
                            Instant.now());
                    DomainEvent domainEvent3 = new MyBoardEvents.BoardCreated(
                            teamId3,
                            boardId3,
                            boardName3,
                            id3,
                            Instant.now());

                    Properties config = new Properties();
                    config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
                    config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                    config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                    KafkaProducer<String, String> producer = new KafkaProducer<>(config);

                    try {
                        producer.send(new ProducerRecord<>(TOPIC, Json.asString(
                                new DomainEventDto(
                                        domainEvent1,
                                        "Board",
                                        Instant.now())))).get();
                        producer.send(new ProducerRecord<>(TOPIC, Json.asString(
                                new DomainEventDto(
                                        domainEvent2,
                                        "Board",
                                        Instant.now())))).get();
                        producer.send(new ProducerRecord<>(TOPIC, Json.asString(
                                new DomainEventDto(
                                        domainEvent3,
                                        "Board",
                                        Instant.now())))).get();
                    } catch (InterruptedException | ExecutionException e) {
                        throw new RuntimeException(e);
                    } finally {
                        producer.close();
                    }
                })
                .And("the message consumer crashed on the second event", env -> {
                    await().untilAsserted(() -> assertFalse(consumerThread.isAlive()));
                })
                .When("a new consumer with a correct acl consumer started", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                    try {
                        messageConsumer.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    messageConsumer = new KafkaMessageConsumer(externalReactor, host, TOPIC,"1", "1");
                    consumerThread = new Thread(messageConsumer);
                    consumerThread.start();
                })
                .Then("the consumer should receive two remote domain event from Kafka", env -> {
                    await().untilAsserted(() -> assertEquals(2, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);
                    assertEquals("BoardCreated", event.getEventSimpleName());

                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId2, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId2, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName2, jsonNode.at("/boardName").textValue());
                    assertEquals(id2, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    class FakeExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private List<DomainEventDto> executedDomainEventDtos;

        public FakeExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
            executedDomainEventDtos = new ArrayList<>();
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (!this.domainEventDtos.stream().anyMatch(x->x.id().equals(domainEventDto.id()))) {
                this.domainEventDtos.add(domainEventDto);
                executedDomainEventDtos.add(domainEventDto);
            }
        }
    }

    class IncorrectExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private int limitSize = 1;

        public IncorrectExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (this.domainEventDtos.size() >= limitSize) {
                throw new RuntimeException();
            }
            this.domainEventDtos.add(domainEventDto);
        }
    }
}
