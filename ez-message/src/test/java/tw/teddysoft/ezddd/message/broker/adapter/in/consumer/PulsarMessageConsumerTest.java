package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.containers.PulsarContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Testcontainers
public class PulsarMessageConsumerTest {
    private static Feature feature;
    private static Rule relayRule;

    @Container
    PulsarContainer pulsar = new PulsarContainer(
            DockerImageName.parse("apachepulsar/pulsar:3.0.0")).withEnv("PULSAR_PREFIX_brokerDeduplicationEnabled", "true");

    private final String TOPIC = "test";
    private String url;
    private ActiveMessageConsumer messageConsumer;
    private ExternalReactor<DomainEventDto> externalReactor;
    private Thread consumerThread;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Pulsar Message Consumer");
        relayRule = feature.NewRule("A Pulsar consumer basic behavior")
                .description("""
                            In order to ensure eventual consistency among bounded context
                            As a Pulsar consumer
                            I want to receive correct remote domain events and post them to acl consumer
                        """);
    }

    @BeforeEach
    public void setup() {

        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();
        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(typeMapper);

        url = pulsar.getPulsarBrokerUrl();

    }

    @AfterEach
    public void tearDown() throws IOException {
        if (null != consumerThread) {
            consumerThread.interrupt();
            messageConsumer.close();
        }
    }

    @EzScenario
    public void should_forward_an_event_from_Pulsar_to_acl_consumer() {
        String teamId = "teamId";
        String boardId = "boardId";
        String boardName = "boardName";
        UUID id = UUID.randomUUID();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker Pulsar with a topic 'test' already started", env -> {
                })
                .And("an acl consumer is created", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                })
                .And("a consumer connects to the Pulsar and subscribe to the topic 'test' to forward messages to the acl consumer", env -> {
                    try {
                        messageConsumer = new PulsarMessageConsumer(externalReactor, url, "test");
                    } catch (PulsarClientException e) {
                        throw new RuntimeException(e);
                    }
                    consumerThread = new Thread(messageConsumer);
                    consumerThread.start();
                })
                .When("a remote domain event is sent to Pulsar", env -> {
                    DomainEvent domainEvent = new MyBoardEvents.BoardCreated(
                            teamId,
                            boardId,
                            boardName,
                            id,
                            Instant.now());

                    PulsarClient pulsarClient = null;
                    Producer<DomainEventDto> producer = null;
                    try {
                        pulsarClient = PulsarClient.builder().serviceUrl(url).build();
                        producer = pulsarClient.newProducer(Schema.AVRO(DomainEventDto.class)).topic(TOPIC).create();

                        producer.newMessage().value(
                                new DomainEventDto(
                                        domainEvent,
                                        "Board",
                                        Instant.now())).send();
                    } catch (PulsarClientException e) {
                        throw new RuntimeException(e);
                    } finally {
                        try {
                            if (null != pulsarClient) {
                                pulsarClient.close();
                            }
                            if (null != producer) {
                                producer.close();
                            }
                        } catch (PulsarClientException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .Then("the acl consumer should receive a remote domain event from Pulsar", env -> {
                    await().untilAsserted(() -> assertEquals(1, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);
                    assertEquals("BoardCreated", event.getEventSimpleName());

                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName, jsonNode.at("/boardName").textValue());
                    assertEquals(id, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    @EzScenario
    public void should_resend_an_event_after_crash() {
        String teamId1 = "teamId1";
        String teamId2 = "teamId2";
        String teamId3 = "teamId3";
        String boardId1 = "boardId1";
        String boardId2 = "boardId2";
        String boardId3 = "boardId3";
        String boardName1 = "boardName1";
        String boardName2 = "boardName2";
        String boardName3 = "boardName3";
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();
        List<DomainEventDto> receivedDomainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker Pulsar with a topic 'test' is already started", env -> {
                })
                .And("an incorrect external reactor is created", env -> {
                    externalReactor = new IncorrectExternalReactor(receivedDomainEventDtos);
                })
                .And("an consumer connects to the Pulsar and subscribe to the topic 'test' to forward messages to the external reactor", env -> {
                    try {
                        messageConsumer = new PulsarMessageConsumer(externalReactor, url, "test");
                    } catch (PulsarClientException e) {
                        throw new RuntimeException(e);
                    }
                    consumerThread = new Thread(messageConsumer);
                    consumerThread.start();
                })
                .And("three remote domain events are sent to Pulsar", env -> {
                    DomainEvent domainEvent1 = new MyBoardEvents.BoardCreated(
                            teamId1,
                            boardId1,
                            boardName1,
                            id1,
                            Instant.now());
                    DomainEvent domainEvent2 = new MyBoardEvents.BoardCreated(
                            teamId2,
                            boardId2,
                            boardName2,
                            id2,
                            Instant.now());
                    DomainEvent domainEvent3 = new MyBoardEvents.BoardCreated(
                            teamId3,
                            boardId3,
                            boardName3,
                            id3,
                            Instant.now());

                    PulsarClient pulsarClient = null;
                    Producer<DomainEventDto> producer = null;
                    try {
                        pulsarClient = PulsarClient.builder().serviceUrl(url).build();
                        var schema = Schema.AVRO(DomainEventDto.class);
                        producer = pulsarClient.newProducer(schema).topic(TOPIC).create();

                        producer.newMessage().value(
                                new DomainEventDto(
                                        domainEvent1,
                                        "Board",
                                        Instant.now())).send();
                        DomainEventDto domainEventDto2 = new DomainEventDto(
                                domainEvent2,
                                "Board",
                                Instant.now());
                        producer.newMessage().value(
                                domainEventDto2).send();
                                        producer.newMessage().value(
                                new DomainEventDto(
                                        domainEvent3,
                                        "Board",
                                        Instant.now())).send();
                    } catch (PulsarClientException e) {
                        throw new RuntimeException(e);
                    } finally {
                        try {
                            if (null != pulsarClient) {
                                pulsarClient.close();
                            }
                            if (null != producer) {
                                producer.close();
                            }
                        } catch (PulsarClientException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .And("the event bus crashed on the second event", env -> {
                    await().untilAsserted(() -> assertFalse(consumerThread.isAlive()));
                })
                .When("a new handler with a correct acl consumer started", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                    try {
                        messageConsumer.close();
                        messageConsumer = new PulsarMessageConsumer(externalReactor, url, "test");
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    consumerThread = new Thread(messageConsumer);
                    consumerThread.start();
                })
                .Then("the consumer should receive two remote domain event from Pulsar", env -> {
                    await().untilAsserted(() -> assertEquals(2, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);
                    assertEquals("BoardCreated", event.getEventSimpleName());

                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId2, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId2, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName2, jsonNode.at("/boardName").textValue());
                    assertEquals(id2, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    static class FakeExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private List<DomainEventDto> executedDomainEventDtos;

        public FakeExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
            executedDomainEventDtos = new ArrayList<>();
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (!this.domainEventDtos.stream().anyMatch(x->x.id().equals(domainEventDto.id()))) {
                this.domainEventDtos.add(domainEventDto);
                executedDomainEventDtos.add(domainEventDto);
            }
        }
    }

    static class IncorrectExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private int limitSize = 1;

        public IncorrectExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (this.domainEventDtos.size() >= limitSize) {
                throw new RuntimeException();
            }
            this.domainEventDtos.add(domainEventDto);
        }
    }
}
