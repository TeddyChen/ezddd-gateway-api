package tw.teddysoft.ezddd.message.broker.adapter.in.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.message.reactor.adapter.in.ExternalReactor;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventDto;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;
import tw.teddysoft.ezspec.keyword.Rule;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
public class RabbitMQMessageConsumerTest {
    private static Feature feature;
    private static Rule relayRule;


    private final static String TOPIC = "test";

    @Container
    public RabbitMQContainer rabbitMQContainer = new RabbitMQContainer("rabbitmq:3.13");

    private String host;
    private int port;
    private CallbackMessageConsumer messageConsumer;
    private ExternalReactor<DomainEventDto> externalReactor;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("RabbitMQ Message Consumer");
        relayRule = feature.NewRule("A RabbitMQ consumer basic behavior")
                .description("""
                            In order to ensure eventual consistency among bounded context
                            As a RabbitMQ consumer
                            I want to receive correct remote domain events and post them to acl handler
                        """);
    }

    @BeforeEach
    public void setup() {
        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();
        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });
        DomainEventMapper.setMapper(typeMapper);

        host = rabbitMQContainer.getHost();
        port = rabbitMQContainer.getAmqpPort();
    }

    @AfterEach
    public void tearDown() throws IOException {
        if (null != messageConsumer) {
            messageConsumer.close();
        }
    }

    @EzScenario
    public void should_forward_an_event_from_RabbitMQ_to_acl_consumer() {
        String teamId = "teamId";
        String boardId = "boardId";
        String boardName = "boardName";
        UUID id = UUID.randomUUID();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker RabbitMQ with a topic 'test' is already started", env -> {
                })
                .And("an acl handler is created", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                })
                .And("a consumer connects to the RabbitMQ and subscribe to the topic 'test' to forward messages to the acl consumer", env -> {
                    messageConsumer = new RabbitMQMessageConsumer(externalReactor, host, port, TOPIC);
                    messageConsumer.subscribe();
                })
                .When("a remote domain event is sent to RabbitMQ", env -> {
                    DomainEvent domainEvent = new MyBoardEvents.BoardCreated(
                            teamId,
                            boardId,
                            boardName,
                            id,
                            Instant.now());

                    ConnectionFactory factory = new ConnectionFactory();
                    factory.setHost(host);
                    factory.setPort(port);
                    try (Connection connection = factory.newConnection();
                        Channel producerChannel = connection.createChannel()) {
                        producerChannel.queueDeclare(TOPIC, false, false, false, null);
                        producerChannel.basicPublish(
                                "",
                                TOPIC,
                                null,
                                Json.asString(new DomainEventDto(
                                                domainEvent,
                                                "Board",
                                                Instant.now())).getBytes());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } catch (TimeoutException e) {
                        throw new RuntimeException(e);
                    }
                })
                .Then("the consumer should receive a remote domain event from RabbitMQ", env -> {
                    await().untilAsserted(() -> assertEquals(1, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);
                    assertEquals("BoardCreated", event.getEventSimpleName());

                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName, jsonNode.at("/boardName").textValue());
                    assertEquals(id, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    @EzScenario
    public void should_resend_an_event_after_crash() {
        String teamId1 = "teamId1";
        String teamId2 = "teamId2";
        String teamId3 = "teamId3";
        String boardId1 = "boardId1";
        String boardId2 = "boardId2";
        String boardId3 = "boardId3";
        String boardName1 = "boardName1";
        String boardName2 = "boardName2";
        String boardName3 = "boardName3";
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        List<DomainEventDto> incorrectDomainEventDtos = new ArrayList<>();
        List<DomainEventDto> domainEventDtos = new ArrayList<>();

        feature.newScenario().withRule(relayRule)
                .Given("a message broker RabbitMQ with a topic 'test' is already started", env -> {
                })
                .And("an incorrect acl handler is created", env -> {
                    externalReactor = new IncorrectExternalReactor(incorrectDomainEventDtos);
                })
                .And("a consumer connects to the RabbitMQ and subscribe to the topic 'test' to forward messages to the acl consumer", env -> {
                    messageConsumer = new RabbitMQMessageConsumer(externalReactor, host, port, TOPIC);
                    messageConsumer.subscribe();
                })
                .And("three remote domain event are sent to RabbitMQ", env -> {
                    DomainEvent domainEvent1 = new MyBoardEvents.BoardCreated(
                            teamId1,
                            boardId1,
                            boardName1,
                            id1,
                            Instant.now());
                    DomainEvent domainEvent2 = new MyBoardEvents.BoardCreated(
                            teamId2,
                            boardId2,
                            boardName2,
                            id2,
                            Instant.now());
                    DomainEvent domainEvent3 = new MyBoardEvents.BoardCreated(
                            teamId3,
                            boardId3,
                            boardName3,
                            id3,
                            Instant.now());

                    ConnectionFactory factory = new ConnectionFactory();
                    factory.setHost(host);
                    factory.setPort(port);
                    try (Connection connection = factory.newConnection();
                         Channel producerChannel = connection.createChannel()) {
                        producerChannel.queueDeclare(TOPIC, false, false, false, null);
                        producerChannel.basicPublish(
                                "",
                                TOPIC,
                                null,
                                Json.asString(new DomainEventDto(
                                                domainEvent1,
                                                "Board",
                                                Instant.now())).getBytes());
                        producerChannel.basicPublish(
                                "",
                                TOPIC,
                                null,
                                Json.asString(new DomainEventDto(
                                                domainEvent2,
                                                "Board",
                                                Instant.now())).getBytes());
                        producerChannel.basicPublish(
                                "",
                                TOPIC,
                                null,
                                Json.asString(new DomainEventDto(
                                                domainEvent3,
                                                "Board",
                                                Instant.now())).getBytes());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } catch (TimeoutException e) {
                        throw new RuntimeException(e);
                    }
                })
                .And("the acl consumer crashed on the second event", env -> {
                    await().untilAsserted(() -> assertTrue(((IncorrectExternalReactor) externalReactor).isExceptionThrown));
                })
                .When("a new consumer with a correct acl handler started", env -> {
                    externalReactor = new FakeExternalReactor(domainEventDtos);
                    try {
                        messageConsumer.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    messageConsumer = new RabbitMQMessageConsumer(externalReactor, host, port, TOPIC);
                    messageConsumer.subscribe();
                })
                .Then("the consumer should receive two remote domain event from RabbitMQ", env -> {
                    await().untilAsserted(() -> assertEquals(2, domainEventDtos.size()));
                    DomainEventDto event = domainEventDtos.get(0);
                    assertEquals("BoardCreated", event.getEventSimpleName());

                    JsonNode jsonNode = Json.readTree(event.getJsonEvent());
                    assertEquals(teamId2, jsonNode.at("/teamId").textValue());
                    assertEquals(boardId2, jsonNode.at("/boardId").textValue());
                    assertEquals(boardName2, jsonNode.at("/boardName").textValue());
                    assertEquals(id2, UUID.fromString(jsonNode.at("/id").textValue()));
                }).Execute();
    }

    static class FakeExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private List<DomainEventDto> executedDomainEventDtos;

        public FakeExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
            executedDomainEventDtos = new ArrayList<>();
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (!this.domainEventDtos.stream().anyMatch(x->x.id().equals(domainEventDto.id()))) {
                this.domainEventDtos.add(domainEventDto);
                executedDomainEventDtos.add(domainEventDto);
            }
        }
    }

    static class IncorrectExternalReactor implements ExternalReactor<DomainEventDto> {
        private List<DomainEventDto> domainEventDtos;
        private int limitSize = 1;
        private boolean isExceptionThrown = false;

        public IncorrectExternalReactor(List<DomainEventDto> domainEventDtos) {
            this.domainEventDtos = domainEventDtos;
        }

        @Override
        public void execute(DomainEventDto domainEventDto) {
            if (this.domainEventDtos.size() >= limitSize) {
                try {
                    throw new RuntimeException();
                } finally {
                    isExceptionThrown = true;
                }
            }
            this.domainEventDtos.add(domainEventDto);
        }
    }
}
