package tw.teddysoft.ezddd.message.broker.adapter.messagebroker;

import edu.emory.mathcs.backport.java.util.Collections;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.rnorth.ducttape.unreliables.Unreliables;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
public class KafkaTest {

    @Container
    public KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.6.0"));

    @BeforeEach
    public void setup() {
        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();

        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });

        DomainEventMapper.setMapper(typeMapper);
    }

    @Test
    public void test_producer_and_consumer() {
        String host = kafkaContainer.getBootstrapServers();

        System.out.println(host);

        Properties adminProperties = new Properties();
        adminProperties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        AdminClient adminClient = KafkaAdminClient.create(adminProperties);
        NewTopic topic = new NewTopic("test", 1, (short) 1);
        adminClient.createTopics(List.of(topic));
        adminClient.close();

        Properties config = new Properties();
        config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        Properties consumerConfig = new Properties();
        consumerConfig.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        consumerConfig.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfig.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfig.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "1");
        consumerConfig.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        Consumer<String, String> consumer = new KafkaConsumer<>(consumerConfig);

        Producer<String, String> producer = new KafkaProducer<>(config);
        consumer.subscribe(Collections.singletonList("test"));
        try {
            producer.send(new ProducerRecord<>("test", new MyBoardEvents.BoardCreated("teamId",
                    "boardId",
                    "boardName",
                    UUID.randomUUID(),
                    Instant.now()).toString())).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Try Start");

        Unreliables.retryUntilTrue(10, TimeUnit.SECONDS, () -> {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            if (records.isEmpty()) return false;
            assertEquals(1, records.count());
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Received message: " + record.value());
            }
            for (TopicPartition partition : records.partitions()) {
                try {
                    List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
                    long lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                    consumer.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        });
        consumer.close();

        Consumer<String, String> consumer2 = new KafkaConsumer<>(consumerConfig);
        consumer2.subscribe(Collections.singletonList("test"));

        try {
            producer.send(new ProducerRecord<>("test", new MyBoardEvents.BoardRenamed("teamId",
                    "boardId",
                    "boardName2",
                    UUID.randomUUID(),
                    Instant.now()).toString())).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
        producer.close();

        Unreliables.retryUntilTrue(10, TimeUnit.SECONDS, () -> {
            ConsumerRecords<String, String> records = consumer2.poll(Duration.ofMillis(100));
            if (records.isEmpty()) return false;
            assertEquals(1, records.count());
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Received message: " + record.value());
            }
            for (TopicPartition partition : records.partitions()) {
                List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
                long lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                consumer2.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
            }
            consumer2.close();
            return true;
        });
    }

    @Test
    public void test_producer_and_consumer_with_json() {
        String host = kafkaContainer.getBootstrapServers();

        System.out.println(host);

        Properties adminProperties = new Properties();
        adminProperties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        AdminClient adminClient = KafkaAdminClient.create(adminProperties);
        NewTopic topic = new NewTopic("test", 1, (short) 1);
        adminClient.createTopics(List.of(topic));
        adminClient.close();

        Properties config = new Properties();
        config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        Properties consumerConfig = new Properties();
        consumerConfig.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        consumerConfig.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfig.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfig.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "1");
        consumerConfig.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        Consumer<String, String> consumer = new KafkaConsumer<>(consumerConfig);

        Producer<String, String> producer = new KafkaProducer<>(config);
        consumer.subscribe(Collections.singletonList("test"));
        try {
            DomainEventData data = DomainEventMapper.toData(new MyBoardEvents.BoardCreated("teamId",
                    "boardId",
                    "boardName",
                    UUID.randomUUID(),
                    Instant.now()));
            producer.send(new ProducerRecord<>("test", Json.asString(data))).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Try Start");

        Unreliables.retryUntilTrue(10, TimeUnit.SECONDS, () -> {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            if (records.isEmpty()) return false;
            assertEquals(1, records.count());
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Received message: " + record.value());
                DomainEventData data = Json.readValue(record.value(), DomainEventData.class);
                assertTrue(DomainEventMapper.toDomain(data) instanceof MyBoardEvents.BoardCreated);
            }
            for (TopicPartition partition : records.partitions()) {
                try {
                    List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
                    long lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                    consumer.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        });
        consumer.close();

        Consumer<String, String> consumer2 = new KafkaConsumer<>(consumerConfig);

        consumer2.subscribe(Collections.singletonList("test"));

        try {
            producer.send(new ProducerRecord<>("test", new MyBoardEvents.BoardRenamed("teamId",
                    "boardId",
                    "boardName2",
                    UUID.randomUUID(),
                    Instant.now()).toString())).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
        producer.close();

        Unreliables.retryUntilTrue(10, TimeUnit.SECONDS, () -> {
            ConsumerRecords<String, String> records = consumer2.poll(Duration.ofMillis(100));
            if (records.isEmpty()) return false;
            assertEquals(1, records.count());
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Received message: " + record.value());
            }
            for (TopicPartition partition : records.partitions()) {
                List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
                long lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                consumer2.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
            }
            consumer2.close();
            return true;
        });
    }
}
