package tw.teddysoft.ezddd.message.broker.adapter.messagebroker;

import org.apache.pulsar.client.api.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PulsarContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import tw.teddysoft.ezddd.entity.DomainEvent;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
public class PulsarTest {

    @Container
    PulsarContainer pulsar = new PulsarContainer(
            DockerImageName.parse("apachepulsar/pulsar:3.0.0")).withEnv("PULSAR_PREFIX_brokerDeduplicationEnabled", "true");

    @BeforeEach
    public void setup() {
        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();
        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });

        DomainEventMapper.setMapper(typeMapper);
    }


    @Test
    public void test_producer_and_consumer() throws PulsarClientException {
        String url = pulsar.getPulsarBrokerUrl();

        try (PulsarClient pulsarClient = PulsarClient.builder().serviceUrl(url).build();
             Producer<DomainEventData> producer = pulsarClient.newProducer(Schema.JSON(DomainEventData.class)).topic("test").create();
             Consumer consumer = pulsarClient.newConsumer(Schema.JSON(DomainEventData.class)).topic("test").subscriptionName("test").subscribe();
        ) {
            DomainEvent boardCreated = new MyBoardEvents.BoardCreated("teamId",
                    "boardId",
                    "boardName",
                    UUID.randomUUID(),
                    Instant.now());

            DomainEventData data = DomainEventMapper.toData(boardCreated);
            producer.newMessage().value(data).send();
            Message<DomainEventData> message;

            message = consumer.receive();
            DomainEventData receivedData = message.getValue();
            consumer.acknowledge(message);

            DomainEvent receivedEvent = DomainEventMapper.toDomain(receivedData);
            assertEquals(boardCreated, receivedEvent);
        }
    }

    @Test
    public void test_producer_and_consumer_with_idempotent() throws PulsarClientException {
        String url = pulsar.getPulsarBrokerUrl();

        try (PulsarClient pulsarClient = PulsarClient.builder().serviceUrl(url).build();
             Producer<DomainEventData> producer = pulsarClient.newProducer(Schema.JSON(DomainEventData.class)).topic("test").create();
             Consumer consumer = pulsarClient.newConsumer(Schema.JSON(DomainEventData.class)).topic("test").subscriptionName("test").subscribe();
        ) {
            DomainEvent boardCreated = new MyBoardEvents.BoardCreated("teamId",
                    "boardId",
                    "boardName",
                    UUID.randomUUID(),
                    Instant.now());

            DomainEventData data = DomainEventMapper.toData(boardCreated);
            var msg = producer.newMessage().value(data);
            msg.send();
            msg.send();

            Messages<DomainEventData> messages = consumer.batchReceive();
            consumer.acknowledge(messages);
            assertEquals(2, messages.size());
        }
    }
}
