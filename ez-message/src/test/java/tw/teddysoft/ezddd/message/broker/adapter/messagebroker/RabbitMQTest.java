package tw.teddysoft.ezddd.message.broker.adapter.messagebroker;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import tw.teddysoft.ezddd.common.Json;
import tw.teddysoft.ezddd.entity.DomainEventTypeMapper;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventData;
import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.message.broker.adapter.MyBoardEvents;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@Testcontainers
public class RabbitMQTest {
    private final static String QUEUE_NAME = "DomainEvent";

    @Container
    public RabbitMQContainer rabbitMQContainer = new RabbitMQContainer("rabbitmq:3.13-management");

    @BeforeEach
    public void setup() {
        System.out.println("Rabbit mq http port: " + rabbitMQContainer.getHttpPort());
        DomainEventTypeMapper typeMapper = DomainEventTypeMapper.create();

        MyBoardEvents.mapper().getMap().forEach((key, value) -> {
            typeMapper.put(key, value);
        });

        DomainEventMapper.setMapper(typeMapper);
    }

    @Test
    public void test_producer_and_consumer() {
        List<DomainEventData> datas = new ArrayList<>();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitMQContainer.getHost());
        factory.setPort(rabbitMQContainer.getAmqpPort());
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            DomainEventData data = DomainEventMapper.toData(new MyBoardEvents.BoardCreated("teamId",
                    "boardId",
                    "boardName",
                    UUID.randomUUID(),
                    Instant.now()));
            channel.basicPublish("", QUEUE_NAME, null, Json.asString(data).getBytes());

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                System.out.println("Receive events ********");
                DomainEventData domainEventData = Json.readAs(delivery.getBody(), DomainEventData.class);
                datas.add(domainEventData);
            };
            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
                System.out.println(consumerTag);
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }

        await().atMost(10, TimeUnit.SECONDS).untilAsserted(() -> assertEquals(1, datas.size()));
        assertTrue(DomainEventMapper.toDomain(datas.get(0)) instanceof MyBoardEvents.BoardCreated);
    }
}
