package tw.teddysoft.ezddd.data.io.ezoutbox;

import org.springframework.transaction.annotation.Transactional;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox.OutboxData;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageData;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDataMapper;
import tw.teddysoft.ezddd.data.io.ezes.store.MessageDbClient;

import java.util.List;
import java.util.Optional;

public class EzOutboxClient<T extends OutboxData, ID> {

    private final OrmClient<T, ID> ormClient;
    private final MessageDbClient messageDbClient;
    private final String EVENT_TYPE_PREFIX = "$et-";

    public EzOutboxClient(OrmClient<T, ID> ormClient, MessageDbClient messageDbClient) {
        this.ormClient = ormClient;
        this.messageDbClient = messageDbClient;
    }

    @Transactional
    public long save(T data) {
        if (data.getDomainEventDatas().size() == 0) {
            return data.getVersion();
        }
        long version = ormClient.saveAndUpdateVersion(data);
        messageDbClient.saveDomainEventsWithoutVersion(new EventStoreData(data.getStreamName(), data.getVersion(), data.getDomainEventDatas()));
        return version;
    }

    public Optional<T> findById(ID id) {
        return ormClient.findById(id);
    }

    public Optional<EventStoreData> getEventsByType(String eventType) {
        return getEventsByStreamName(EVENT_TYPE_PREFIX + eventType);
    }

    public Optional<EventStoreData> getEventsByStreamName(String streamName) {
        EventStoreData eventStoreData = new EventStoreData();
        eventStoreData.setStreamName(streamName);
        eventStoreData.setVersion(-1);
        List<MessageData> messageDatas = messageDbClient.findByStreamName(streamName);

        messageDatas.forEach(x -> {
            eventStoreData.getDomainEventDatas().add(MessageDataMapper.toDomainEventData(x));
        });

        return Optional.of(eventStoreData);
    }

    @Transactional
    public void delete(T data) {
        ormClient.deleteById((ID) data.getId());
        messageDbClient.saveDomainEventsWithoutVersion(new EventStoreData(data.getStreamName(), data.getVersion(), data.getDomainEventDatas()));
    }
}
