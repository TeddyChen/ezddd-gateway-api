package tw.teddysoft.ezddd.data.io.ezoutbox;

import tw.teddysoft.ezddd.data.adapter.repository.es.EventStore;
import tw.teddysoft.ezddd.data.adapter.repository.outbox.OutboxStore;
import org.springframework.transaction.annotation.Transactional;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox.OutboxData;

import java.util.Optional;

public class EzOutboxStoreAdapter<T extends OutboxData, ID> implements OutboxStore<T, ID>, EventStore {

    private final EzOutboxClient<T, ID> client;

    protected EzOutboxStoreAdapter(EzOutboxClient<T, ID> client) {
        this.client = client;
    }

    public static EventStore createUnmodifiableEventStore(EzOutboxClient client) {
        return new EzOutboxStoreAdapter<>(client);
    }

    public static <T extends OutboxData> OutboxStore<T, String> createOutboxStore(EzOutboxClient client) {
        return new EzOutboxStoreAdapter<>(client);
    }

    @Override
    @Transactional
    public long save(T data) {
        return client.save(data);
    }

    @Override
    public void save(EventStoreData eventStoreData) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<T> findById(ID id) {
        return client.findById(id);
    }

    @Override
    public Optional<EventStoreData> getEventsByType(String eventType) {
        return client.getEventsByType(eventType);
    }

    @Override
    public Optional<EventStoreData> getEventsByStreamName(String streamName) {
        return client.getEventsByStreamName(streamName);
    }

    @Override
    @Transactional
    public void delete(T data) {
        client.delete(data);
    }
}
