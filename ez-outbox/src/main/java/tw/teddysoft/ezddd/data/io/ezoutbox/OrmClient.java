package tw.teddysoft.ezddd.data.io.ezoutbox;

import tw.teddysoft.ezddd.usecase.port.out.repository.impl.StoreData;

import java.util.Optional;

public interface OrmClient<T extends StoreData, ID> {
    long saveAndUpdateVersion(T outboxData);
    void updateVersion(ID id, long version);

    Optional<T> findById(ID id);

    void deleteById(ID id);
}
