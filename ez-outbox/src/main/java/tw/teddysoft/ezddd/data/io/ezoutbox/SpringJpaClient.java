package tw.teddysoft.ezddd.data.io.ezoutbox;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.transaction.annotation.Transactional;
import tw.teddysoft.ezddd.data.adapter.repository.OptimisticLockingFailureException;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.StoreData;

public interface SpringJpaClient<T extends StoreData<ID>, ID> extends OrmClient<T, ID>, CrudRepository<T, ID> {

    @Transactional
    default long saveAndUpdateVersion(T outboxData) {
        long version = outboxData.getVersion();
        outboxData.setVersion(outboxData.getOptimisticLockVersion());
        try {
            save(outboxData);
        } catch (ObjectOptimisticLockingFailureException e) {
            throw new OptimisticLockingFailureException(e);
        }
        if (outboxData.getDomainEventDatas().size() > 1) {
            updateVersion(outboxData.getId(), version);
        }

        return version;
    }

    @Modifying
    @Transactional
    @Query(value = "UPDATE #{#entityName} as c SET c.version = :version WHERE c.id = :id")
    void updateVersion(@Param("id") ID id, @Param("version") long version);
}
