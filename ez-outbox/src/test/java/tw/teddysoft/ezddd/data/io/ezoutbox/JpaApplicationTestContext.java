package tw.teddysoft.ezddd.data.io.ezoutbox;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@EntityScan(basePackages={"tw.teddysoft.ezddd.data.io.ezoutbox"})
@SpringBootApplication(scanBasePackages={"tw.teddysoft.ezddd.data.io.ezoutbox"})
public abstract class JpaApplicationTestContext {
}
