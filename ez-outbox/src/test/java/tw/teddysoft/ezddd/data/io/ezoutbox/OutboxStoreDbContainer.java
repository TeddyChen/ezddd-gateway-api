package tw.teddysoft.ezddd.data.io.ezoutbox;

import com.github.dockerjava.api.model.HealthCheck;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class OutboxStoreDbContainer<SELF extends OutboxStoreDbContainer<SELF>> extends GenericContainer<SELF> {
    private static final String REGISTRY;
    private static final String IMAGE;
    private static final String VERSION;
    private static final HealthCheck HEALTH_CHECK;

    private static final String DATA_PATH;
//    private static final String INDEX_PATH;
    private static final String LOG_PATH;

    static {
        HEALTH_CHECK = new HealthCheck()
                .withInterval(1000000000L)
                .withTimeout(1000000000L)
                .withRetries(10);

        String architecture = System.getProperty("os.arch");
        if (architecture.equals("aarch64")) {
            REGISTRY = "ezkanban/postgres_message_db";
            IMAGE = "postgres_test";
            VERSION = "2.0-arm64v8";
        } else {
            REGISTRY = "ezkanban/postgres_message_db";
            IMAGE = "postgres_test";
            VERSION = "2.0";
        }

        DATA_PATH = "/var/lib/postgresql";
//        INDEX_PATH = "/etc/eventstore";
        LOG_PATH = "/var/log/postgresql";
    }

    public OutboxStoreDbContainer() {
        super(String.format("%s/%s:%s", REGISTRY, IMAGE, VERSION));

        addExposedPorts(5432, 6000);

//        withEnv("EVENTSTORE_RUN_PROJECTIONS", "ALL");
//        withEnv("EVENTSTORE_ENABLE_ATOM_PUB_OVER_HTTP", "true");
//        withEnv("EVENTSTORE_INSECURE", "true");
//        withEnv("EVENTSTORE_START_STANDARD_PROJECTIONS", "true");
//        withEnv("EVENTSTORE_ENABLE_EXTERNAL_TCP", "true");
//        withEnv("EVENTSTORE_MAX_APPEND_SIZE", "16767216");

        withCreateContainerCmdModifier(cmd -> cmd.withHealthcheck(HEALTH_CHECK));
        waitingFor(Wait.forHealthcheck());
    }

    public SELF bindDbPath(String path) {
        return withFileSystemBind(path + "/data", DATA_PATH, BindMode.READ_WRITE)
//                .withFileSystemBind(path + "/index", INDEX_PATH, BindMode.READ_WRITE)
                .withFileSystemBind(path + "/log", LOG_PATH, BindMode.READ_WRITE);
    }
}