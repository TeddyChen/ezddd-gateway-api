package tw.teddysoft.ezddd.data.io.ezoutbox;

import org.jetbrains.annotations.NotNull;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Set;

public class PostgreSqlMessageDbContainer<SELF extends PostgreSqlMessageDbContainer<SELF>> extends JdbcDatabaseContainer<SELF> {
    private static final DockerImageName DEFAULT_IMAGE_NAME;
    private static final String DEFAULT_TAG;
    public static final Integer POSTGRESQL_PORT = 5432;
    static final String DEFAULT_USER = "postgres";
    static final String DEFAULT_PASSWORD = "root";
    static final String DEFAULT_DB = "postgres";
    private String databaseName;
    private String username;
    private String password;

    static {
        DEFAULT_IMAGE_NAME = DockerImageName.parse("ezkanban/postgres_message_db");

        String architecture = System.getProperty("os.arch");
        if (architecture.equals("aarch64")) {
            DEFAULT_TAG = "arm64v8";
        } else {
            DEFAULT_TAG = "2.0";
        }
    }

    public PostgreSqlMessageDbContainer(String databaseName) {
        this(DEFAULT_IMAGE_NAME.withTag(DEFAULT_TAG), databaseName);
    }

    public PostgreSqlMessageDbContainer(String dockerImageName, String databaseName) {
        this(DockerImageName.parse(dockerImageName), databaseName);
    }

    public PostgreSqlMessageDbContainer(DockerImageName dockerImageName, String databaseName) {
        super(dockerImageName);
        this.databaseName = databaseName;
        this.urlParameters.put("currentSchema", "message_store");
        this.username = DEFAULT_USER;
        this.password = DEFAULT_PASSWORD;
        dockerImageName.assertCompatibleWith(new DockerImageName[]{DEFAULT_IMAGE_NAME});

        this.waitStrategy = (new LogMessageWaitStrategy())
                .withRegEx(".*listening on IPv4 address.*\\s")
                .withTimes(1)
                .withStartupTimeout(Duration.of(100L, ChronoUnit.SECONDS));
        this.setCommand(new String[]{"postgres", "-c", "fsync=off"});
        this.addExposedPort(POSTGRESQL_PORT);
    }

    /** @deprecated */
    @Deprecated
    protected @NotNull Set<Integer> getLivenessCheckPorts() {
        return super.getLivenessCheckPorts();
    }

    protected void configure() {
        this.withUrlParam("loggerLevel", "OFF");
        this.addEnv("POSTGRES_DB", DEFAULT_DB);
        this.addEnv("POSTGRES_USER", this.username);
        this.addEnv("POSTGRES_PASSWORD", this.password);
    }

    public String getDriverClassName() {
        return "org.postgresql.Driver";
    }

    public String getJdbcUrl() {
        String additionalUrlParams = this.constructUrlParameters("?", "&");
        return "jdbc:postgresql://" + this.getHost() + ":" + this.getMappedPort(POSTGRESQL_PORT) + "/" + this.databaseName + additionalUrlParams;
    }

    public String getDatabaseName() {
        return this.databaseName;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getTestQueryString() {
        return "SELECT 1";
    }

    public SELF withDatabaseName(String databaseName) {
        this.databaseName = databaseName;
        return this.self();
    }

    public SELF withUsername(String username) {
        this.username = username;
        return this.self();
    }

    public SELF withPassword(String password) {
        this.password = password;
        return this.self();
    }

    protected void waitUntilContainerStarted() {
        this.getWaitStrategy().waitUntilReady(this);
    }

    public SELF bindDbPath(String hostPath) {
        return bindDbPath(hostPath, BindMode.READ_WRITE);
    }
    public SELF bindDbPath(String hostPath, BindMode bindMode) {
        this.withFileSystemBind(
                hostPath,
                "/var/lib/postgresql/data",
                bindMode);
        return this.self();
    }
}
