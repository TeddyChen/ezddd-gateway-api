package tw.teddysoft.ezddd.data.io.ezoutbox;

import tw.teddysoft.ezddd.entity.EsAggregateRoot;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static tw.teddysoft.ucontract.Contract.*;

public class User extends EsAggregateRoot<String, UserEvents> {

	public final static String CATEGORY = "User";
	private String userId;
	private String username;
	private String password;
	private String email;
	private String nickname;
	private Role role;
	private boolean isActivated;

	//region :Constructors
	public User(List<UserEvents> domainEvents){
		super(domainEvents);
	}

	public User(String userId, String username, String password, String email, String nickname, Role role, boolean isActivated) {
		super();
		apply(new UserEvents.UserRegistered(userId, username, password, email, nickname, role, isActivated, UUID.randomUUID(), Instant.now()));
	}
	//endregion

	//region :Behaviors
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();

		sb.append("id=").append(getId()).append(",")
				.append("username=").append(this.username).append(",")
				.append("nickname=").append(this.nickname).append(",")
				.append("email=").append(this.email).append(",")
				.append("activated=").append(this.isActivated);

		return sb.toString();
	}

	public void markAsDeleted(String userId) {
		apply(new UserEvents.UserDeleted(userId, UUID.randomUUID(), Instant.now()));
	}

	public void changeEmail(String newEmail) {
		apply(new UserEvents.EmailChanged(userId, newEmail, UUID.randomUUID(), Instant.now()));
	}

	public void changeNickname(String newNickname) {
		if (reject("nick name equals", () -> getNickname().equals(newNickname))) return;

		apply(new UserEvents.NicknameChanged(userId, newNickname, UUID.randomUUID(), Instant.now()));
	}

	public void changePassword(String newPassword) {
		apply(new UserEvents.PasswordChanged(userId, newPassword, UUID.randomUUID(), Instant.now()));
	}

	public String getEmail() {
		return email;
	}

	public String getNickname() {
		return nickname;
	}

	public String getPassword() {
		return password;
	}

	public Role getRole() {
		return role;
	}

	public String getUsername() {
		return username;
	}

	public void activate(String executorUserId) {
		if (reject("is already activated", () -> isActivated())) return;

		apply(new UserEvents.UserActivated(executorUserId, userId, UUID.randomUUID(), Instant.now()));
	}

	public void deactivate(String executorUserId) {
		if (reject("is already deactivated", () -> !isActivated())) return;

		apply(new UserEvents.Unregistered(executorUserId, userId, UUID.randomUUID(), Instant.now()));
	}

	public boolean isActivated() {
		return isActivated;
	}

	public void loginFail() {
		apply(new UserEvents.UserLoggedInFail(userId, UUID.randomUUID(), Instant.now()));
	}

	public void loginSuccess() {
		apply(new UserEvents.UserLoggedIn(userId, UUID.randomUUID(), Instant.now()));
	}

	@Override
	public boolean isDeleted() {
		return isDeleted;
	}

	@Override
	public String getCategory() {
		return CATEGORY;
	}

	@Override
	public String getId() {
        return this.userId;
    }

    @Override
    public void ensureInvariant() {
		invariant("is not marked as deleted", () -> !isDeleted());
		invariant(format("CATEGORY '%s'", getCategory()), () -> getCategory() == CATEGORY);
		invariantNotNull("User Id", getId());
		invariantNotNull("User name", getUsername());
		invariantNotNull("Password", getPassword());
		invariantNotNull("Email", getEmail());
		invariantNotNull("Nickname", getNickname());
		invariantNotNull("Role", getRole());
	}

	@Override
	protected void when(UserEvents event) {
		switch (event) {
			case UserEvents.UserRegistered e -> whenUserRegistered(e);
			case UserEvents.UserDeleted e -> whenUserDeleted(e);
			case UserEvents.EmailChanged e -> whenEmailChanged(e);
			case UserEvents.NicknameChanged e -> whenNicknameChanged(e);
			case UserEvents.PasswordChanged e -> whenPasswordChanged(e);
			case UserEvents.UserActivated e -> whenUserActivated(e);
			case UserEvents.Unregistered e -> whenUnregistered(e);
			default -> {}
		}
	}

	private void whenUnregistered(UserEvents.Unregistered e) {
		this.isActivated = false;
	}

	private void whenUserActivated(UserEvents.UserActivated e) {
		this.isActivated = true;
	}

	private void whenPasswordChanged(UserEvents.PasswordChanged e) {
		this.password = e.password();
	}

	private void whenNicknameChanged(UserEvents.NicknameChanged e) {
		this.nickname = e.nickname();
	}

	private void whenEmailChanged(UserEvents.EmailChanged e) {
		this.email = e.email();
	}

	private void whenUserDeleted(UserEvents.UserDeleted e) {
		this.isDeleted = true;
	}

	private void whenUserRegistered(UserEvents.UserRegistered e) {
		this.userId = e.userId();
		this.username = e.username();
		this.password = e.password();
		this.email = e.email();
		this.nickname = e.nickname();
		this.role = e.role();
		this.isDeleted = false;
		this.isActivated = e.isActivated();
	}
	//endregion
}
