package tw.teddysoft.ezddd.data.io.ezoutbox;

import tw.teddysoft.ezddd.usecase.port.inout.domainevent.DomainEventMapper;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox.OutboxMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserMapper {

    public static List<User> toDomain(List<UserData> datas) {
        List<User> users = new ArrayList<>();
        datas.forEach(x -> users.add(toDomain(x)));
        return users;
    }

    public static User toDomain(UserData data) {
        User user = new User(data.getId(), data.getUsername(), data.getPassword(), data.getEmail(), data.getNickname(), Role.valueOf(data.getRole()), data.isActivated());

        user.clearDomainEvents();
        return user;
    }

    public static Optional<User> toDomain(Optional<UserData> data) {
        return data.isPresent() ? Optional.of(toDomain(data.get())) : Optional.empty();
    }

    public static UserData toData(User user) {
        UserData data = new UserData();
        data.setId(user.getId());
        data.setUsername(user.getUsername());
        data.setEmail(user.getEmail());
        data.setPassword(user.getPassword());
        data.setNickname(user.getNickname());
        data.setRole(user.getRole().toString());
        data.setActivated(user.isActivated());
        data.setStreamName(user.getStreamName());
        data.setDomainEventDatas(user.getDomainEvents().stream().map(DomainEventMapper::toData).collect(Collectors.toList()));
        data.setVersion(user.getVersion());
        return data;
    }

    private static OutboxMapper mapper = new Mapper();

    public static OutboxMapper newMapper() {
        return mapper;
    }

    static class Mapper implements OutboxMapper<User, UserData> {

        @Override
        public User toDomain(UserData data) {
            return UserMapper.toDomain(data);
        }

        @Override
        public UserData toData(User aggregateRoot) {
            return UserMapper.toData(aggregateRoot);
        }
    }
}
