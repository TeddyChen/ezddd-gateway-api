package tw.teddysoft.ezddd.data.io.ezoutbox;

public interface UserSpringJpaClient extends SpringJpaClient<UserData, String> {
}
