package tw.teddysoft.ezddd.data.adapter.in.relay;

import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

public abstract class ActiveEventRelay <EventInDatabase, Event, EventProducer extends MessageProducer<Event>>
        extends EventRelay<EventInDatabase, Event, EventProducer>
        implements Runnable {

    public ActiveEventRelay(EventProducer eventProducer, Converter<EventInDatabase, Event> converter) {
        super(eventProducer, converter);
    }

    public abstract void shutdown();
}
