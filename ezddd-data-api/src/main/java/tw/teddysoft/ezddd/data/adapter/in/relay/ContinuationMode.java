package tw.teddysoft.ezddd.data.adapter.in.relay;

public enum ContinuationMode {
    CONTINUE, TERMINATION
}
