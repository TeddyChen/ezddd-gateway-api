package tw.teddysoft.ezddd.data.adapter.in.relay;

import tw.teddysoft.ezddd.common.Converter;
import tw.teddysoft.ezddd.usecase.port.inout.messaging.MessageProducer;

/**
 * {@code EventRelay} is an abstract class representing the ability to copy
 * events from an aggregate store to an Event Bus or Event Broker.
 * Its subclasses should acknowledge the aggregate store once the event is
 * copied to the Event Bus or Event Broker.
 *
 * @param <EventInDatabase> is the native event type read from database
 * @param <Event> is the event type that is posted to EventBus
 * @param <EventProducer> is a component that posts events to an Event Bus
 *                       or Event Broker.
 *
 * @author Teddy Chen
 * @author ezKanban team
 * @since 0.0.3
 * @see MessageProducer#post(Event) MessageProducer
 */
abstract class EventRelay<EventInDatabase, Event, EventProducer extends MessageProducer<Event>> {

    protected final EventProducer eventProducer;

    protected final Converter<EventInDatabase, Event> converter;

    protected ContinuationMode continuationMode;

    public EventRelay(EventProducer eventProducer, Converter<EventInDatabase, Event> converter) {
        this.eventProducer = eventProducer;
        this.converter = converter;
        this.continuationMode = ContinuationMode.CONTINUE;
    }

}
