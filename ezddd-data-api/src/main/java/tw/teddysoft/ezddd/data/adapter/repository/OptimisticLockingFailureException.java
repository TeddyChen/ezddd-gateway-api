package tw.teddysoft.ezddd.data.adapter.repository;

public class OptimisticLockingFailureException extends RuntimeException{
    public OptimisticLockingFailureException(Exception e) {
        super(e);
    }
}
