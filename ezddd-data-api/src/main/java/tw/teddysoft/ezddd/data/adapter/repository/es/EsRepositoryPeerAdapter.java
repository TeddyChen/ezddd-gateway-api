package tw.teddysoft.ezddd.data.adapter.repository.es;

import tw.teddysoft.ezddd.data.adapter.repository.OptimisticLockingFailureException;
import tw.teddysoft.ezddd.usecase.port.out.repository.RepositorySaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeerSaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;

import java.util.Optional;

import static tw.teddysoft.ucontract.Contract.requireNotNull;

public class EsRepositoryPeerAdapter implements RepositoryPeer<EventStoreData, String> {

    private final EventStore eventStore;

    public EsRepositoryPeerAdapter(EventStore eventStore) {
        requireNotNull("EsdbStore", eventStore);
        this.eventStore = eventStore;
    }

    @Override
    public Optional<EventStoreData> findById(String aggregateStreamName) {
        return eventStore.getEventsByStreamName(aggregateStreamName);
    }

    @Override
    public void save(EventStoreData eventStoreData) {
        try {
            eventStore.save(eventStoreData);
        } catch (OptimisticLockingFailureException e) {
            throw new RepositoryPeerSaveException(RepositorySaveException.OPTIMISTIC_LOCKING_FAILURE, e);
        }
    }

    @Override
    public void delete(EventStoreData data) {
        this.save(data);
    }
}
