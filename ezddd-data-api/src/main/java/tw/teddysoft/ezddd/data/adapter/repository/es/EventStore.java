package tw.teddysoft.ezddd.data.adapter.repository.es;

import tw.teddysoft.ezddd.usecase.port.out.repository.impl.es.EventStoreData;

import java.util.Optional;

public interface EventStore {

    String EVENT_TYPE_PREFIX = "$et-";

    void save(EventStoreData eventStoreData);

    Optional<EventStoreData> getEventsByType(String eventType);

    Optional<EventStoreData> getEventsByStreamName(String streamName);
}
