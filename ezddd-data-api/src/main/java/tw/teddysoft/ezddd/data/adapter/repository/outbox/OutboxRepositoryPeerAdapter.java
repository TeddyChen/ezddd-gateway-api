package tw.teddysoft.ezddd.data.adapter.repository.outbox;

import tw.teddysoft.ezddd.data.adapter.repository.OptimisticLockingFailureException;
import tw.teddysoft.ezddd.usecase.port.out.repository.RepositorySaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeer;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.RepositoryPeerSaveException;
import tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox.OutboxData;

import java.util.Optional;

public class OutboxRepositoryPeerAdapter<T extends OutboxData, ID> implements RepositoryPeer<T, ID> {

    private final OutboxStore<T, ID> outboxStore;

    public OutboxRepositoryPeerAdapter(OutboxStore outboxStore) {
        this.outboxStore = outboxStore;
    }

    @Override
    public void save(T data) {
        try {
            long version = outboxStore.save(data);
            data.setVersion(version);
        } catch (OptimisticLockingFailureException e) {
            throw new RepositoryPeerSaveException(RepositorySaveException.OPTIMISTIC_LOCKING_FAILURE, e);
        }
    }

    @Override
    public Optional<T> findById(ID id) {
        return outboxStore.findById(id);
    }

    @Override
    public void delete(T data) {
        outboxStore.delete(data);
    }
}
