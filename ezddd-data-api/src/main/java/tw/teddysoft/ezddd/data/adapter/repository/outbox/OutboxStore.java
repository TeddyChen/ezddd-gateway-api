package tw.teddysoft.ezddd.data.adapter.repository.outbox;

import tw.teddysoft.ezddd.usecase.port.out.repository.impl.outbox.OutboxData;

import java.util.Optional;

public interface OutboxStore<T extends OutboxData, ID> {

    long save(T data);

    Optional<T> findById(ID id);

    void delete(T data);
}
